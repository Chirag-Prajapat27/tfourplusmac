import 'dart:ui';

import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';

typedef onTapCallBack = void Function();

class CustomButton extends StatefulWidget {
  final onTapCallBack onTapButton;
  String buttonTitle = "";
  double? height = 50;
  Color? backGroundColor = AppColor.colorBlack;
  bool? isBorder = false;
  double borderRadius = 5;
  Color? boxShadowColor = Colors.transparent;
  double? blurRadius = 0;



  CustomButton(
      {Key? key,
      required this.buttonTitle,
      this.height = 50,
      this.isBorder = false,
      this.backGroundColor = AppColor.colorBlack,
        this.borderRadius = 5,
        this.boxShadowColor = Colors.transparent,
        this.blurRadius = 0,
      required this.onTapButton})
      : super(key: key);

  @override
  _CustomButtonState createState() {
    buttonTitle = this.buttonTitle;
    height = this.height;
    backGroundColor = this.backGroundColor;

    return _CustomButtonState();
  }
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onTapButton();
      },
      child: Container(
        // constraints: BoxConstraints(maxWidth: 300),
        height: widget.height,
        decoration: new BoxDecoration(
          color: widget.backGroundColor,
          border: Border.all(
              width: 1.0,
              color: widget.isBorder!
                  ? AppColor.colorPrimary
                  : Colors.transparent),
          borderRadius: new BorderRadius.all(Radius.circular(widget.borderRadius)),
          boxShadow: [
            BoxShadow(
              color: widget.boxShadowColor!,
              blurRadius: widget.blurRadius!,
              offset: Offset(0, 5), // Shadow position
            ),
          ]
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(widget.buttonTitle,
                style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.semiBold,
                    color: AppColor.colorPrimary)),
          ],
        ),
      ),
    );
  }
}
