import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(child: Container(
        height: 150,
        width: 150,
        decoration: BoxDecoration(
          color: Colors.white
        ),
        child: Center(child: CupertinoTheme( data:
        CupertinoTheme.of(context).copyWith(brightness: Brightness.light),
          child: CupertinoActivityIndicator(radius: 15), ),),
        // child: Center(child: CupertinoActivityIndicator()),
      )),
      // body: Center(child: SpinKitCircle(color: Colors.white)),
    );
  }
}