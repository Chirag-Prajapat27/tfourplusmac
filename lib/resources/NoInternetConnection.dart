import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';

import 'CustomButton.dart';

class NoInternetConnection extends StatefulWidget {

  const NoInternetConnection({Key? key,this.requestCode}) : super(key: key);

  final String? requestCode;

  @override
  _NoInternetConnectionState createState() => _NoInternetConnectionState();
}

class _NoInternetConnectionState extends State<NoInternetConnection> {

  bool isInternet = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    checkInternet();
    super.initState();
  }

  Future<void> checkInternet()  async {
    isInternet = await Utilities.isConnectedNetwork();
    if (isInternet) {
      // Navigator.
      Navigator.pop(context,isInternet);
    }
    else
      Utilities.showSnackBar(context, "Please check your internet connection");

  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(AppImages.noInternetConnection,height: 100, width: 150),
                SizedBox(height: 10),
                Text('Please check internet connection',style: Utilities.setTextStyle(AppFontWeight.titleSubHeader, AppFontWeight.semiBold,color: AppColor.colorBlack),),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: CustomButton(buttonTitle: "Try Again", onTapButton:  () async {
                    checkInternet();}),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    bool isInternetCon = await Utilities.isConnectedNetwork();
    if (isInternetCon) {
      // Navigator.
      Navigator.pop(context,true);
      // return Future.value(true);
    }
    else{
      Utilities.showSnackBar(context, "Please check your internet connection");
      return Future.value(false);
    }

    return Future.value(false);
  }
}
