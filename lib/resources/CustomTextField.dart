import 'dart:ui';

import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';


// typedef onTextChangeCallBack = void Function();

class CustomTextField extends StatefulWidget {
  final Function onTextChange;
  String hintText = "";
  TextEditingController controller;
  String errorMessage = "";
  double? height = 60;
  int? maxLength = 140;

  bool? isShowEye;
  TextInputAction? textInputAction = TextInputAction.next;
  TextInputType? textInputType = TextInputType.text;
  Color? backGroundColor = AppColor.colorPrimary;
  bool? autofocus = false;
  bool? obscureText = false;
  bool? isBorder = false;
  double borderRadius = 5;
  Color? boxShadowColor = AppColor.colorGray;
  double? blurRadius = 8;
  Function? onTapEye;

  CustomTextField(
      {Key? key,
      required this.hintText,
      required this.controller,
      this.height = 60,
      this.maxLength = 140,
      this.isBorder = false,

      this.isShowEye = false,
      this.errorMessage = '',
      this.obscureText = false,
      this.autofocus = false,
      this.textInputAction = TextInputAction.next,
      this.textInputType = TextInputType.text,
      this.backGroundColor = AppColor.colorPrimary,
      this.borderRadius = 5,
      this.boxShadowColor = AppColor.colorGray,
      this.blurRadius = 8,
      this.onTapEye,
      required this.onTextChange})
      : super(key: key);

  @override
  _CustomTextField createState() {
    hintText = this.hintText;
    height = this.height;
    controller = this.controller;
    backGroundColor = this.backGroundColor;

    return _CustomTextField();
  }
}

class _CustomTextField extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.centerLeft,
          height: widget.height,
          child: TextField(
            controller: widget.controller,
            maxLength: widget.maxLength,
            onChanged: (text) {
              setState(() {
                widget.errorMessage = "";
              });
            },
            decoration: InputDecoration(
                border: InputBorder.none,
                counterText: '',
                hintText: widget.hintText,
                suffixIcon: widget.isShowEye! ? Padding(padding: EdgeInsets.fromLTRB(0, 0, 4, 0),
                child: GestureDetector(
                  onTap: (){ widget.onTapEye!(); },
                  child: Icon(widget.obscureText! ? Icons.visibility_rounded :
                  Icons.visibility_off_rounded, size: 24),
                ),
                ) :Icon(Icons.add, size: 0,),
                contentPadding:
                EdgeInsets.fromLTRB(15, 15, 15, 10)),
            autofocus: widget.autofocus!,
            obscureText: widget.obscureText!,
            keyboardType: widget.textInputType!,
            textInputAction: widget.textInputAction!,

          ),
          decoration: BoxDecoration(
            color: widget.backGroundColor,
            borderRadius: BorderRadius.circular(widget.borderRadius),
            boxShadow: [
              BoxShadow(
                color: widget.boxShadowColor!,
                blurRadius: widget.blurRadius!,
                offset: Offset(0, 5), // Shadow position
              ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Text(widget.errorMessage,
          style: Utilities.setTextStyle(
              AppFontWeight.subTitleText, AppFontWeight.regular,
              color: Colors.red),
        ),
      ],
    );
  }
}
