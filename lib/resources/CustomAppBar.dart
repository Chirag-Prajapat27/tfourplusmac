
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';

typedef OnBackCall = void Function();
// typedef changeLanBtn = void Function();

class CustomeAppBar extends StatefulWidget {
   CustomeAppBar({Key? key,required this.onTabMenu,required this.title}) : super(key: key);

   OnBackCall onTabMenu;
    String title;


  @override
  State<CustomeAppBar> createState() => _CustomeAppBarState();
}

class _CustomeAppBarState extends State<CustomeAppBar> {
  @override
  Widget build(BuildContext context) {
      return Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
                onTap: widget.onTabMenu,
                child: Icon(Icons.arrow_back_ios,
                    color: AppColor.colorBlack, size: 24)),
            Text(widget.title,
                style: Utilities.setTextStyle(
                    AppFontWeight.titleText, AppFontWeight.semiBold,
                    color: AppColor.colorBlack)),
            SizedBox(width: 15)
          ],
        ),
        decoration: BoxDecoration(
          color: AppColor.colorPrimary,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 3.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
      );

  }
}
