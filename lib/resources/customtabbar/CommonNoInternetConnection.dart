import 'dart:ui';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';

import '../CustomButton.dart';


class CommonNoInternetConnection extends StatefulWidget {
  @override
  _CommonNoInternetConnectionState createState() => _CommonNoInternetConnectionState();
}

class _CommonNoInternetConnectionState extends State<CommonNoInternetConnection> {

  bool isInternet = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    checkInternet();
    super.initState();
  }

  Future<void> checkInternet()  async {
    isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
    } else {
      Navigator.pop(context, true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Column(
            children: [
              Container(
                width: MediaQuery. of(context). size. width,
                height: MediaQuery. of(context). size. height - 100,
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // SvgPicture.asset(
                            //   AppImages.noInternet,
                            //   height: MediaQuery. of(context). size. width/3,
                            //   width: MediaQuery. of(context). size. width/3,
                            // ),
                            SizedBox(height: 30.0,),
                            Text("Whoops!", style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                color: AppColor.colorBlack,
                                fontSize: 20),),
                            SizedBox(height: 16.0,),
                            Text("Check your internet connection or try again.", style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                                color: AppColor.colorBlack,
                                fontSize: 15),textAlign: TextAlign.center,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16.0,),
              CustomButton(buttonTitle: 'Try Again', onTapButton: () {
                checkInternet();
              }, backGroundColor: AppColor.colorBlack),
            ],
          ),
        ],
      ),
    );
  }

}