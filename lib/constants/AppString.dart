class AppStrings
{

  //Static message of APP
  static String next = 'Next';
  static String login ='Log in';

  static String featureProduct ='Feature Products';
  static String featureProductAr ='المنتجات المميزة';

  static String latestProject ='Latest Project';
  static String latestProjectAr ='أحدث مشروع';

  static String headerName ='tfourplus';

  static String company ='Company';
  static String companyAr ='شركة';

  static String vision ='Vision';
  static String visionAr ='رؤية';

  static String mission ='Mission';
  static String missionAr ='مهمة';

  static String submitCap = 'SUBMIT';
  static String submit ='submit';
  static String submitAr ='إرسال';

  static String contactUs ='Contact Us';
  static String contactUsAr ='اتصل بنا';

  static String ourTeam ='Our Team';
  static String ourTeamAr ='فريقنا';

  static String ourProject ='Our Project';
  static String ourProjectAr ='مشروعنا';

  static String home ='Home';
  static String homeAr ='مسكن';

  static String project ='Projects';
  static String projectAr ='المشاريع';

  static String warehouse ='WareHouse';
  static String warehouseAr ='مستودع';

  static String aboutUs ='About Us';
  static String aboutUsAr ='معلومات عنا';

  static String products ='Products';
  static String productsAr ='منتجات';

  static String contact ='Contact';
  static String contactAr ='اتصل';

  static String signUp ='Sing Up';
  static String signUpAr ='اشتراك';

  static String logout ='Logout';
  static String logoutAr ='تسجيل خروج';

  static String cancel ='Cancel';
  static String cancelAr ='يلغي';

  static String logoutDialogTitle = 'Are you sure you want to Logout.';
  static String logoutDialogTitleAr = 'هل أنت متأكد أنك تريد تسجيل الخروج.';

  static String enterName ='Enter your name';
  static String enterEmail = 'Enter your email ID';
  static String message ='Message';
  static String enterPassword ='Enter password';
  static String retypePassword ='Retype password';

  static String emptyName ='Please enter Full Name';
  static String emptyEmail = 'Please enter email';
  static String validEmail = 'Please enter valid email';
  static String emptyMessage ='Please enter message';
  static String emptyPassword ='Please enter password';
  static String emptyConfirmPassword ='Please enter Confirm password';
  static String passwordDoesNotMatch ='Password does not match';
  static String min6LetterPassword ='Please enter minimum 6 letter password';

  static String alreadyHaveAnAccount = 'Already have an account? ';

}