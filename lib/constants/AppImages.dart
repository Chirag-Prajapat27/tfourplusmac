
class AppImages{
// All assets images
  static String morePankh = 'assets/image/morepankh.jpeg';
  static String menu = 'assets/image/menu.png';
  static String closeBtn = 'assets/image/closeBtnIcon.png';
  static String appleBlack24dp = 'assets/image/apple_black_24dp.svg';
  static String lanChange = 'assets/image/lanchange.svg';
  static String whatAppIcon = 'assets/image/WhatsAppicon.svg';
  static String circleRightArrow = 'assets/image/circlerightarrow.svg';
  static String noInternetConnection = 'assets/image/noConnectioncCoud.png';
  static String twitter = 'assets/image/twitter.png';
  static String facBookIcon = 'assets/image/facebook-circular-logo.png';
  static String youTubeIcon = 'assets/image/youtube.png';
  static String logo = 'assets/image/Logo01.svg';
  static String logoTFour = 'assets/image/logoTFour.png';
  static String whatsAppBlack = 'assets/image/whatsapp_Black_circle.png';


}