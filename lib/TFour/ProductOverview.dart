import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tfour/Model/SubItemCategory.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/resources/CustomButton.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import '../api/RequestCode.dart';
import '../module/ApiPresenter.dart';
import 'Dashboard.dart';

class ProductOverview extends StatefulWidget {
  const ProductOverview({Key? key, required this.list}) : super(key: key);

  final SubItemCategory list;

  @override
  _ProductOverviewState createState() => _ProductOverviewState();
}

class _ProductOverviewState extends State<ProductOverview> implements ApiCallBacks {
  bool _isLoading = false;
  bool lanAr = false;
  TextEditingController tfName = TextEditingController();
  TextEditingController tfEmailAddress = TextEditingController();
  TextEditingController tfMessage = TextEditingController();
  List<String> errorMessageList = ['', '', ''];
  final CarouselController _slideController = CarouselController();
  List<String> slidImageList = [];
  int _current = 0;

  @override
  void initState() {
    getLanguage();
    addSliderImage();
    super.initState();
  }

  getLanguage() async {
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ? true : false;
  }

  addSliderImage() {
    slidImageList.add(widget.list.proImgUrl.toString());
    slidImageList.add(widget.list.s1ImgUrl.toString());
    slidImageList.add(widget.list.s2ImgUrl.toString());
    slidImageList.add(widget.list.s3ImgUrl.toString());
    slidImageList.add(widget.list.s4ImgUrl.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: AppColor.colorPrimary,
          body: SafeArea(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      setImage(),
                      fieldsForm(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                      onTap: () {
                        Navigation.pop(context);
                      },
                      child: Icon(Icons.arrow_back_ios,
                          color: AppColor.colorBlack, size: 24)),
                ),
              ],
            ),
          ),
        ),
        _isLoading ? Loader() : Container()
      ],
    );
  }

  setImage() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CarouselSlider(
          options: CarouselOptions(
              height: 250,
              initialPage: 0,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              autoPlayInterval: Duration(seconds: 5),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
          items: slidImageList.map((item) => CachedNetworkImage(
            imageUrl: IMAGE_BASE_URL + item,
            fit: BoxFit.fill,
            width: MediaQuery.of(context).size.width,
            height: 250,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Center(
                        child: Container(height: 150, width: 150,
                          decoration: BoxDecoration(color: Colors.transparent),
                          child: Center(
                              child: CupertinoActivityIndicator(
                                radius: 15,
                              )),
                        ))),
            errorWidget: (context, url, error) => Icon(Icons.error, size: 50),
          ),
          )
              .toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: slidImageList
              .asMap()
              .entries
              .map((entry) {
            return GestureDetector(
              onTap: () => _slideController.animateToPage(entry.key),
              child: Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme
                        .of(context)
                        .brightness == Brightness.dark
                        ? Colors.white
                        : Colors.black)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4)),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }


  fieldsForm() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              lanAr
                  ? widget.list.proNameAr.toString()
                  : widget.list.proName.toString(),
              style: Utilities.setTextStyle(
                  AppFontWeight.subHeader, AppFontWeight.bold)),
          SizedBox(height: 10),
          Text('Description :',
              style: Utilities.setTextStyle(
                  AppFontWeight.titleText, AppFontWeight.semiBold)),
          SizedBox(height: 10),
          Text(
              lanAr
                  ? widget.list.proDesc.toString()
                  : widget.list.proDesc.toString(),
              // maxLines: ,
              style: Utilities.setTextStyle(
                  AppFontWeight.subTitleText, AppFontWeight.semiBold, height: 1.4)),
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              border: Border.all(color: Colors.black, width: 1),
            ),
            child: InkWell(
              onTap: () {
                if (widget.list.pdf1Url != null) {
                  getDownloadPDF();
                } else {
                  Utilities.showSnackBar(context, "PDF is not available");
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(Icons.upload_file),
                      SizedBox(width: 5),
                      Text('Download',
                          style: Utilities.setTextStyle(
                              AppFontWeight.titleSubHeader,
                              AppFontWeight.semiBold)),
                    ],
                  ),
                  SvgPicture.asset(AppImages.circleRightArrow,
                      color: AppColor.colorBlack, width: 24, height: 24)
                ],
              ),
            ),
          ),
          SizedBox(height: 22),
          Text('Enquiry Project',
              style: Utilities.setTextStyle(
                  AppFontWeight.subTitleText, AppFontWeight.semiBold)),
          SizedBox(height: 10),
          TextField(
              controller: tfName,
              onChanged: (text) {
                errorMessageList[1] = "";
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  counterText: "",
                  hintText: 'Enter your Name',
                  labelText: 'Enter your Name'),
              autofocus: false,
              obscureText: false,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next),
          SizedBox(height: 5),
          Text(
            errorMessageList[0],
            style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular,
                color: Colors.red),
          ),
          SizedBox(height: 10),
          TextField(
              controller: tfEmailAddress,
              onChanged: (text) {
                errorMessageList[1] = "";
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.colorBlack)),
                  counterText: "",
                  hintText: 'Enter your Email',
                  labelText: 'Enter your Email'),
              autofocus: false,
              obscureText: false,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next),
          SizedBox(height: 5),
          Text(
            errorMessageList[1],
            style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular,
                color: Colors.red),
          ),
          SizedBox(height: 10),
          TextField(
              controller: tfMessage,
              onChanged: (text) {
                errorMessageList[2] = "";
              },
              textAlign: TextAlign.start,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.colorBlack)),
                  counterText: "",
                  hintText: 'Enter your message'),
              maxLines: 5,
              autofocus: false,
              obscureText: false,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.done),
          SizedBox(height: 5),
          Text(
            errorMessageList[2],
            style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular,
                color: Colors.red),
          ),
          SizedBox(height: 10),
          CustomButton(
            buttonTitle: lanAr ? AppStrings.submitAr : AppStrings.submit,
            onTapButton: () {
              if (formValidation()) {
                setState(() {
                  ApiPresenter(this).contactForm(
                      tfName.text.toString(),
                      tfEmailAddress.text.toString(),
                      tfMessage.text.toString(),
                      context);
                  _isLoading = true;
                });
              }
            },
          ),
        ],
      ),
    );
  }

  bool formValidation() {
    bool isValid = true;
    setState(() {
      for (int v = 0; v < errorMessageList.length; v++) {
        if (v == 0) {
          if (tfName.text.toString().trim().isEmpty) {
            errorMessageList[0] = AppStrings.emptyName;
            isValid = false;
          } else
            errorMessageList[0] = '';
        } else if (v == 1) {
          if (tfEmailAddress.text.toString().isEmpty) {
            errorMessageList[1] = AppStrings.emptyEmail;
            isValid = false;
          } else {
            if (!Utilities.validateEmail(
                tfEmailAddress.text.toString().trim())) {
              errorMessageList[1] = AppStrings.validEmail;
              isValid = false;
            } else
              errorMessageList[1] = '';
          }
        } else if (v == 2) {
          if (tfMessage.text.toString().trim().isEmpty) {
            errorMessageList[2] = AppStrings.emptyMessage;
            isValid = false;
          } else {
            errorMessageList[2] = '';
          }
        }
      }
    });
    return isValid;
  }

  void getDownloadPDF() {
    String path = Utilities.getDownloadPath() as String;
    Utilities.downloadFile(widget.list.pdf1Url!, widget.list.proName!, path);
  }

  @override
  void onConnectionError(String error, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if (value) {
        initState();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    Utilities.showSnackBar(context, errorMsg);

  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    if (requestCode == RequestCode.CONTACT_FORM) {
      setState(() {
        Navigation.pushAndRemoveUntil(context, Dashboard());
          Utilities.showSnackBar(context, strMsg);
        _isLoading = false;
      });
    }
  }
}
