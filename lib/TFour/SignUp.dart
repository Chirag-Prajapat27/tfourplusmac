import 'package:flutter/material.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/resources/CustomTextField.dart';
import '../api/ApiInterface.dart';
import '../api/RequestCode.dart';
import '../constants/AppFontWeight.dart';
import '../constants/AppImages.dart';
import '../module/ApiPresenter.dart';
import '../resources/CustomButton.dart';
import '../resources/Loader.dart';
import '../resources/NoInternetConnection.dart';
import '../utility/ScreenNavigation.dart';
import '../utility/Utilities.dart';
import 'Login.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> implements ApiCallBacks {
  TextEditingController tfFullName = TextEditingController();
  TextEditingController tfEmailAddress = TextEditingController();
  TextEditingController tfPassword = TextEditingController();
  TextEditingController tfConfirmPassword = TextEditingController();
  List<String> errorMessageList = ['', '', '', ''];
  bool _isLoading = false;
  bool isShowPass = true;
  bool isShowRePass = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: SafeArea(
          child: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Column(
                children: [
                  SizedBox(height: 40),
                  Image.asset(AppImages.logoTFour),
                  SizedBox(height: 20),
                  Text(
                    "Sign Up",
                    style: Utilities.setTextStyle(
                        AppFontWeight.Header, AppFontWeight.regular,
                        color: AppColor.colorBlack),
                  ),
                  SizedBox(height: 30),
                  CustomTextField(
                      hintText: AppStrings.enterName,
                      controller: tfFullName,
                      errorMessage: errorMessageList[0],
                      borderRadius: 20,
                      onTextChange: () {}),
                  SizedBox(height: 12),
                  CustomTextField(
                      hintText: AppStrings.enterEmail,
                      controller: tfEmailAddress,
                      errorMessage: errorMessageList[1],
                      textInputType: TextInputType.emailAddress,
                      borderRadius: 20,
                      onTextChange: () {}),
                  SizedBox(height: 12),
                  CustomTextField(
                      hintText: AppStrings.enterPassword,
                      controller: tfPassword,
                      obscureText: isShowRePass,
                      errorMessage: errorMessageList[2],
                      textInputType: TextInputType.visiblePassword,
                      borderRadius: 20,
                      isShowEye: true,
                      onTapEye: () {
                        setState(() {
                          isShowRePass = !isShowRePass;
                        });
                      },
                      onTextChange: () {}),
                  SizedBox(height: 12),
                  CustomTextField(
                      hintText: AppStrings.retypePassword,
                      controller: tfConfirmPassword,
                      obscureText: isShowPass,
                      errorMessage: errorMessageList[3],
                      textInputType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.done,
                      borderRadius: 20,
                      isShowEye: true,
                      onTapEye: () {
                        setState(() {
                          isShowPass = !isShowPass;
                        });
                      },
                      onTextChange: () {}),
                  SizedBox(height: 30),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
                    child: CustomButton(
                      borderRadius: 40,
                      buttonTitle: AppStrings.submitCap,
                      boxShadowColor: AppColor.colorGray,
                      blurRadius: 8,
                      onTapButton: () {
                        if (formValidation()) {
                          getApiData();
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 20),
                  InkWell(
                    onTap: () => Navigation.pushAndRemoveUntil(context, Login()),
                    child: RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: AppStrings.alreadyHaveAnAccount,
                          style: Utilities.setTextStyle(
                              AppFontWeight.subTitleText, AppFontWeight.regular)),
                      TextSpan(
                          text: AppStrings.login,
                          style: Utilities.setTextStyle(
                              AppFontWeight.titleText, AppFontWeight.bold,
                              color: Colors.lightBlueAccent))
                    ])),
                  )
                ],
              ),
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      )),
    );
  }

  bool formValidation() {
    bool isValid = true;
    setState(() {
      for (int v = 0; v < errorMessageList.length; v++) {
        if (v == 0) {
          if (tfFullName.text.toString().isEmpty) {
            errorMessageList[0] = AppStrings.emptyName;
            isValid = false;
          } else {
            errorMessageList[0] = '';
          }
        }
        if (v == 1) {
          if (tfEmailAddress.text.toString().isEmpty) {
            errorMessageList[1] = AppStrings.emptyEmail;
            isValid = false;
          } else {
            if (!Utilities.validateEmail(tfEmailAddress.text.toString().trim())) {
              errorMessageList[1] = AppStrings.validEmail;
              isValid = false;
            } else
              errorMessageList[1] = '';
          }
        } else if (v == 2) {
          if (tfPassword.text.toString().trim().isEmpty) {
            errorMessageList[2] = AppStrings.emptyPassword;
            isValid = false;
          } else {
            if (tfPassword.text.length < 6) {
              errorMessageList[2] = AppStrings.min6LetterPassword;
              isValid = false;
            } else
              errorMessageList[2] = '';
          }
        } else if (tfConfirmPassword.text.trim().isEmpty) {
          errorMessageList[3] = AppStrings.emptyConfirmPassword;
          isValid = false;
        } else {
          if (tfPassword.text.trim() != tfConfirmPassword.text.trim()) {
            errorMessageList[3] = AppStrings.passwordDoesNotMatch;
            isValid = false;
          } else
            errorMessageList[3] = '';
        }
      }
    });
    return isValid;
  }

  /*Call Register Api*/
  getApiData() async {
    setState(() {
      _isLoading = true;
    });
    ApiPresenter(this).signUp(
        name: tfFullName.text.trim().toString(),
        email: tfEmailAddress.text.trim().toString(),
        password: tfPassword.text.trim().toString(),
        phoneType: Utilities.getDeviceType(context));
  }

  @override
  void onConnectionError(String error, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if (value) {
        initState();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    if (requestCode == RequestCode.SIGN_UP) {
      setState(() {
        Utilities.showSnackBar(context, errorMsg);
        _isLoading = false;
      });
    }
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    if (requestCode == RequestCode.SIGN_UP) {
      setState(() {
        Utilities.showSnackBar(context, strMsg);
        Navigation.pushAndRemoveUntil(context, Login());
        _isLoading = false;
      });
    }
  }
}
