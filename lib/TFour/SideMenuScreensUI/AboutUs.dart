
import 'package:tfour/Model/AboutUsModel.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppGif.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  State<AboutUs> createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> implements ApiCallBacks {
  bool _isLoading = false;
  List<AboutUsModel> aboutUsList = [];
  bool lanAr = false;

  @override
  void initState() {
    super.initState();
    getApi();
  }

  void getApi() async {
    setState(() {
      _isLoading = true;
    });
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ?true : false;
    ApiPresenter(this).getAboutUs(context);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: Stack(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                appBar(),
                SizedBox(height: 20),
                Expanded(
                  child: Stack(
                    children: [
                      if (aboutUsList.isEmpty) Center(
                        child: Image.asset(AppGif.noDataFoundGIF,filterQuality: FilterQuality.medium,
                            width: MediaQuery.of(context).size.width),
                      ) else SingleChildScrollView(
                        child:Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 30),
                          child:
                          Column(
                            crossAxisAlignment: lanAr? CrossAxisAlignment.end :CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: DelayedDisplay(
                                  delay: Duration(seconds: 1),
                                  child: Text(lanAr?AppStrings.companyAr : AppStrings.company,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.Header, AppFontWeight.bold)),
                                )
                              ),
                              SizedBox(height: 40),

                              DelayedDisplay(
                                delay: Duration(milliseconds: 1400),
                                child: Text(lanAr?aboutUsList[0].aboutAr.toString():aboutUsList[0].aboutUsContent.toString(),
                                    textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.titleText, AppFontWeight.regular,height: 1.5,latterSpacing: 0.3)),
                              ),
                              SizedBox(height: 35),

                              DelayedDisplay(
                                delay: Duration(milliseconds: 1600),
                                child: Text(lanAr?AppStrings.missionAr:AppStrings.mission,
                                    textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.titleHeader, AppFontWeight.semiBold)),
                              ),
                              SizedBox(height: 4),
                              DelayedDisplay(
                                delay: Duration(milliseconds: 1700),
                                child: Text(lanAr?aboutUsList[0].missionTextAr.toString():aboutUsList[0].missionText.toString(),
                                    textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.titleText, AppFontWeight.regular,height: 1.5,latterSpacing: 0.3)),
                              ),
                              SizedBox(height: 35),
                              DelayedDisplay(
                                delay: Duration(milliseconds: 1900),
                                child: Text(lanAr?AppStrings.visionAr:AppStrings.vision,
                                    textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.titleHeader, AppFontWeight.semiBold)),
                              ),
                              SizedBox(height: 4),
                              DelayedDisplay(
                                delay: Duration(milliseconds: 2100),
                                child: Text(lanAr?aboutUsList[0].visionTextAr.toString():aboutUsList[0].visionText.toString(),
                                    textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                                    style: Utilities.setTextStyle(
                                        AppFontWeight.titleText, AppFontWeight.regular,height: 1.5,latterSpacing: 0.3)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      ),
    );
  }

  appBar() {
    return Stack(
      children: [
        CachedNetworkImage(
          height: 300,
          imageUrl: IMAGE_BASE_URL + aboutUsList[0].aboutImgUrl.toString(),
          fit: BoxFit.fill,
          width: MediaQuery.of(context).size.width,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              Center(
                child: CircularProgressIndicator(
                    value: downloadProgress.progress),
              ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ) ,
        Padding(
          padding: const EdgeInsets.only(left: 16,right: 16,top: 50),
          child: InkWell(
              onTap: () {
                Navigation.pop(context);
              },
              child: Icon(Icons.arrow_back_ios,
                  color: AppColor.colorBlack, size: 24)),
        ),
      ],
    );

  }

  @override
  void onConnectionError(String error, String requestCode) {
    print("Api Error:  $error");
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if(value){
        initState();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    print("Api Error:  $errorMsg");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    print("Success: $strMsg");
    print("object: $object");

    if(requestCode == RequestCode.ABOUT_US){
      setState(() {
        aboutUsList.addAll((object as List).map((data) => AboutUsModel.fromJson(data)));
        _isLoading = false;
      });
    }
  }

}
