
import 'package:auto_animated/auto_animated.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfour/Model/project_products.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppGif.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/CustomAppBar.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import '../ProjectDetails.dart';

class ProjectNew extends StatefulWidget {
  const ProjectNew({Key? key}) : super(key: key);

  @override
  _ProjectNewState createState() => _ProjectNewState();
 }

class _ProjectNewState extends State<ProjectNew> implements ApiCallBacks {
  bool _isLoading = false;
  bool lanAr = false;
  List<ProjectProducts> allProject = [];

  @override
  void initState() {
    super.initState();
    getCategoryData();
  }

  @override
  void dispose() {
    super.dispose();
    allProject.clear();
  }

  getCategoryData() async {
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ?true : false;
    setState(() {
      _isLoading = true;
    });
    ApiPresenter(this).getAllProject(context);
  }

  final options = LiveOptions(
    // Start animation after (default zero)
    delay: Duration(milliseconds: 500),
    visibleFraction: 0.05,
    // Repeat the animation of the appearance
    // when scrolling in the opposite direction (default false)
    // To get the effect as in a showcase for ListView, set true
    reAnimateOnVisibility: false,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: Stack(
        children: [
          SafeArea(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomeAppBar(onTabMenu: ()=> Navigation.pop(context), title: AppStrings.project),
                  SizedBox(height: 20),
                  Expanded(
                    child: SingleChildScrollView(
                      child: allProject.isNotEmpty ? allFields(): Center(
                        child: Image.asset(AppGif.noDataFoundGIF,filterQuality: FilterQuality.medium,
                          height: MediaQuery.of(context).size.height/2,
                          width: MediaQuery.of(context).size.width),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      ),
    );
  }

  allFields() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          LiveList.options(
            options: options,
            itemCount: allProject.length,
            shrinkWrap: true,
            controller: ScrollController(keepScrollOffset: false),
            // Like ListView.builder, but also includes animation property
            itemBuilder: (context, index, Animation<double> animation) =>
            // For example wrap with fade transition
            FadeTransition(
              opacity: Tween<double>(begin: 0, end: allProject.length.toDouble()).animate(animation),
              // And slide transition
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: Offset(0, -0.1),
                  end: Offset.zero,
                ).animate(animation),
                // Paste you Widget
                child: categoryView(index),
              ),
            ),
            // Other properties correspond to the
            // `ListView.builder` / `ListView.separated` widget
            scrollDirection: Axis.vertical,
          ),
        ],
      ),
    );
  }

  categoryView(int index) {
    return InkWell(
      onTap: () {
        Navigation.push(context, ProjectDetails(list: allProject[index]));
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: Stack(
          alignment: Alignment.center,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(7),

                child: CachedNetworkImage(
                  imageUrl: IMAGE_BASE_URL + allProject[index].projImgUrl.toString(),
                  fit: BoxFit.fill,
                  height: MediaQuery.of(context).size.height/ 3,
                  width: MediaQuery.of(context).size.width,

                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      Padding(
                    padding: const EdgeInsets.symmetric(horizontal:50),
                          child: Center(child: Container(height: 150, width: 150,
                            decoration: BoxDecoration(color: Colors.transparent),
                            child: Center(child: CupertinoActivityIndicator(radius: 12)),
                          ))                    // child: CircularProgressIndicator(
                    //     value: downloadProgress.progress),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )),
            Center(
              child: Text(lanAr?allProject[index].projNameAr.toString():allProject[index].projName.toString(),
                  style: Utilities.setTextStyle(
                      AppFontWeight.subHeader, AppFontWeight.semiBold,
                      color: AppColor.colorPrimary)),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onConnectionError(String error, String requestCode) {
    Utilities.showSnackBar(context, error);
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if(value){
        getCategoryData();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    Utilities.showSnackBar(context, errorMsg);
    print("Api Error $errorMsg");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    print("Success $strMsg");
    print("Success $object");
    if (requestCode == RequestCode.ALL_PROJECT) {
      setState(() {
        allProject = (object as List).map((data) => ProjectProducts.fromJson(data)).toList();
        _isLoading = false;
      });
    }
  }
}