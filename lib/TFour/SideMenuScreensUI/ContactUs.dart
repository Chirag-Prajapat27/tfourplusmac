
import 'package:tfour/Model/ContactUsModel.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/CustomAppBar.dart';
import 'package:tfour/resources/CustomButton.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  State<ContactUs> createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> implements ApiCallBacks {

  bool _isLoading = false;
  bool lanAr = false;
  List<ContactUsModel?>? contactUsList = [];
  TextEditingController tfName = TextEditingController();
  TextEditingController tfEmailAddress = TextEditingController();
  TextEditingController tfMessage = TextEditingController();
  List<String> errorMessageList = ['', '', ''];

  @override
  void initState() {
    super.initState();
    getContact();
  }

  getContact() async{
    setState(() {
      _isLoading = true;
    });
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ? true : false;
    ApiPresenter(this).getContactUsInfo(context);

  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: Stack(
        children: [
          SafeArea(
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomeAppBar(onTabMenu: ()=> Navigation.pop(context),
                        title: lanAr?AppStrings.companyAr:AppStrings.contactUs),
                    Column(
                      children: [
                        companyInfo(),
                        // Divider(endIndent: 20,indent: 20,thickness: 1.2),
                        // SizedBox(height: 10),
                        allMedia(),
                        SizedBox(height: 10),
                        Divider(endIndent: 20,indent: 20,thickness: 1.2),
                        SizedBox(height: 20),
                        formContact(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      ),
    );
  }

  companyInfo() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Row(
            //   // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //   children: [
            //   Visibility(
            //       visible: lanAr?false:true,
            //       child: Icon(Icons.location_on_rounded)),
            //   SizedBox(height: 5),
            //     RichText(
            //       textDirection: lanAr?TextDirection.rtl:TextDirection.ltr,
            //       overflow: TextOverflow.ellipsis,
            //       textAlign: TextAlign.center,
            //       maxLines: 3,
            //       text: TextSpan(
            //         style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold),
            //         text: lanAr ? contactUsList![0]!.addressAr.toString() : contactUsList![0]!.address.toString(),
            //       ),
            //     ),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                  Visibility(
                      visible: lanAr?false:true,
                      child: Icon(Icons.location_on_rounded)),
                  SizedBox(width: 5),
                  Flexible(child: Text(lanAr ?contactUsList![0]!.addressAr.toString() : contactUsList![0]!.address.toString(),
                    style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold),
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.start,
                    textDirection: lanAr?TextDirection.rtl:TextDirection.ltr,
                    maxLines: 4,
                  ),),
                    SizedBox(width: 5),
                    Visibility(
                      visible: lanAr?true:false,
                      child: Icon(Icons.location_on_rounded)),
                ],),

                // SizedBox(
                //   width: MediaQuery.of(context).size.width/1,
                //   child: Text(lanAr?"Address "+ contactUsList[0].addressAr.toString():"Address "+contactUsList[0].address.toString(),
                //   style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold),overflow: TextOverflow.ellipsis,maxLines: 3),
                // ),

                // SizedBox(width: 5),
                // Visibility(
                //     visible: lanAr?true:false,
                //     child: Icon(Icons.location_on_rounded)),
              // ],),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: lanAr?MainAxisAlignment.center : MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
               Icon(Icons.phone_android_rounded),
                SizedBox(width: 5),
                Text(contactUsList![0]!.telPhone1.toString()+", "+contactUsList![0]!.telPhone2.toString(),
                  style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold),),

              ],),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: lanAr?MainAxisAlignment.center : MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
               Icon(Icons.mail_rounded),
                SizedBox(width: 5),
                Text(contactUsList![0]!.eMail.toString(),
                  style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold),),
              ],),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  formContact(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
        TextField(
          controller: tfName,
            onChanged: (text){
              errorMessageList[0] = "";
            },
            maxLength: 50,
            decoration: InputDecoration(
              focusColor: Colors.black,
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black)),
                counterText: '',
                hintText: 'Enter your Name',
                labelText: 'Enter your Name'),
            autofocus: false,
            obscureText: false,
            keyboardType: TextInputType.text,textInputAction: TextInputAction.next),
        SizedBox(height: 5),
        Text(errorMessageList[0],
          style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular, color: Colors.red),),
        SizedBox(height: 12),

        TextField(
          controller: tfEmailAddress,
            maxLength: 140,
            onChanged: (text){
              errorMessageList[1] = "";
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColor.colorBlack)),
                counterText: '',
                hintText: 'Enter your Email',
                labelText: 'Enter your Email'),
            autofocus: false,
            obscureText: false,
            keyboardType: TextInputType.emailAddress,textInputAction: TextInputAction.next),
        SizedBox(height: 5),
        Text(errorMessageList[1],
          style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular, color: Colors.red),),
        SizedBox(height: 12),

        TextField(
          maxLength: 255,
            onChanged: (text){
              errorMessageList[2] = "";
            },
          controller: tfMessage,
            textAlign: TextAlign.start,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColor.colorBlack)),
                counterText: '',
                hintText: 'Enter your message'),
            maxLines: 5,
            autofocus: false,
            obscureText: false,
            keyboardType: TextInputType.emailAddress,textInputAction: TextInputAction.done),
        SizedBox(height: 5),
        Text(errorMessageList[2],
          style: Utilities.setTextStyle(AppFontWeight.subTitleText, AppFontWeight.regular, color: Colors.red),),
        SizedBox(height: 12),

        CustomButton(
          buttonTitle: lanAr?AppStrings.submitAr:AppStrings.submit,
          onTapButton: () {
            if(formValidation()){
              setState(() {
                ApiPresenter(this).contactForm(tfName.text.toString(), tfEmailAddress.text.toString(), tfMessage.text.toString(), context);
                _isLoading = true;
              });
            }
          },
        ),
      ],),
    );
  }

  bool formValidation(){
    bool isValid = true;
    setState(() {
      for (int v = 0; v < errorMessageList.length; v++) {
        if (v == 0) {
          if (tfName.text.toString().trim().isEmpty) {
            errorMessageList[0] = AppStrings.emptyName;
            isValid = false;
          } else
            errorMessageList[0] = '';
        } else if (v == 1) {
          if (tfEmailAddress.text.toString().isEmpty) {
            errorMessageList[1] = AppStrings.emptyEmail;
            isValid = false;
          } else {
            if (!Utilities.validateEmail(
                tfEmailAddress.text.toString().trim())) {
              errorMessageList[1] = AppStrings.validEmail;
              isValid = false;
            } else
              errorMessageList[1] = '';
          }
        } else if (v == 2) {
          if (tfMessage.text.toString().trim().isEmpty) {
            errorMessageList[2] = AppStrings.emptyMessage;
            isValid = false;
          } else {
            errorMessageList[2] = '';
          }
        }
        }
    });
     return isValid;
  }

  allMedia() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 60),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                  onTap: ()=> Utilities.launchYouTube(contactUsList![0]!.fbLink.toString()),
                  child: Image.asset(AppImages.facBookIcon,width: 30,height: 30)),
              SizedBox(width: 10),
              InkWell(
                  onTap: ()=> Utilities.launchYouTube(contactUsList![0]!.twitterLink.toString()),
                  child: Image.asset(AppImages.twitter,width: 30,height: 30)),
              SizedBox(width: 10),
              InkWell(
                  onTap: (){
                    Utilities.launchYouTube( contactUsList![0]!.youtubeLink.toString());
                  },
                  child: Image.asset(AppImages.youTubeIcon,width: 35,height: 35)),
              SizedBox(width: 10),
              InkWell(
                  onTap: (){
                    Utilities.openWhatsapp(context, contactUsList![0]!.whatsappNumber.toString());
                  },
                  child: Image.asset(AppImages.whatsAppBlack,width: 30,height: 30)),

            ]),
      ),
    );
  }


  @override
  void onConnectionError(String error, String requestCode) {
    Utilities.showSnackBar(context, error);
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if(value){
        getContact();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    print("Error $errorMsg");
    setState(() {
      _isLoading = false;
    });

  }


  @override
  void onSuccess(object, String strMsg, String requestCode) {

    if(requestCode == RequestCode.CONTACT_US_FOOTER)
    {
      setState(() {
        contactUsList = ((object as List).map((contactData) => ContactUsModel.fromJson(contactData)).toList());
        _isLoading = false;
      });
    }

    if(requestCode == RequestCode.CONTACT_FORM)
    {
      setState(() {
        Utilities.showSnackBar(context, "Sent Successfully");
        _isLoading = false;
      });
    }

  }

}
