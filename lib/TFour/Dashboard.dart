
import 'package:tfour/Model/ContactUsModel.dart';
import 'package:tfour/Model/LanguageModel.dart';
import 'package:tfour/Model/MainCategories.dart';
import 'package:tfour/Model/UserDataModel.dart';
import 'package:tfour/Model/project_products.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'ProjectDetails.dart';
import 'SideMenu.dart';
import 'SideMenuScreensUI/ContactUs.dart';
import 'SubCategory.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> implements ApiCallBacks {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final CarouselController _controller = CarouselController();
  LanguageModel? dropDownValue;

  bool lanAr = false;

  List<MainCategories> sliderList = [];
  List<MainCategories> categoryList = [];
  List<ProjectProducts> allProject = [];
  List<LanguageModel> languageList = [];
  List<ContactUsModel> contactUsList = [];
  List<UserDataModel> userDataList = [];

  bool _isLoading = false;
  final CarouselController _slideController = CarouselController();
  int _current = 0;

  @override
  void initState() {
    super.initState();
    languageList.add(LanguageModel(1, "English", "en", "US"));
    languageList.add(LanguageModel(2, "Arabic(عربي)", "ar", "AR"));
    getApiData();
  }

  @override
  void dispose() {
    super.dispose();
    allProject.clear();
  }

  final options = LiveOptions(
    delay: Duration(seconds: 1),
    visibleFraction: 0.05,
    reAnimateOnVisibility: false,
  );

  getApiData() async {
    setState(() {
      _isLoading = true;
    });

    int lanValue = await SessionManager.getIntData(SessionManager.language);
    String assessToken = await SessionManager.getStringData(SessionManager.ACCESS_TOKEN);
    lanAr = lanValue != 1 ? true : false;
    ApiPresenter(this).getSlider(context);
    ApiPresenter(this).getParentCategory(context);
    ApiPresenter(this).getAllProject(context);
    ApiPresenter(this).getContactUsInfo(context);
    ApiPresenter(this).getUser(context, assessToken);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      key: _scaffoldKey,
      drawer: SideMenu(),
      body: Stack(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                appBar(),
                Expanded(
                  child: SingleChildScrollView(
                    child: allFields(),
                  ),
                ),
              ],
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Utilities.openWhatsapp(context, contactUsList[0].whatsappNumber.toString());
        },
        tooltip: 'Whats App',
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: SvgPicture.asset(AppImages.whatAppIcon, height: 56, width: 56),
        ),
        // child: SvgPicture.asset(AppImages.whatAppIcon,height: 45,width: 45),
      ),
    );
  }

  appBar() {
    return Container(
      color: AppColor.colorBlack,
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16.0),
          color: AppColor.colorPrimary,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                  onTap: () {
                    _scaffoldKey.currentState!.openDrawer();
                  },
                  child: Image.asset(AppImages.menu,
                      height: 16, color: AppColor.colorBlack)),
              Padding(
                  padding: const EdgeInsets.only(left: 65),
                  child: Image.asset(AppImages.logoTFour, height: 35, width: 120)),
              DropdownButton<LanguageModel>(
                value: dropDownValue,
                underline: Container(),
                isDense: true,
                hint: Text(
                  "LANGUAGE",
                  style: Utilities.setTextStyle(
                      AppFontWeight.subTitleText, AppFontWeight.medium,
                      color: AppColor.colorBlack),
                ),
                iconDisabledColor: AppColor.colorBlack,
                iconEnabledColor: AppColor.colorBlack,
                items: languageList
                    .map((LanguageModel lan) => DropdownMenuItem<LanguageModel>(
                        // alignment: AlignmentDirectional.centerStart,
                        value: lan, // this value show on tags
                        child: Text(lan.lanName.toString())))
                    .toList(),
                onChanged: (LanguageModel? value) {
                  setState(() {
                    dropDownValue = value;
                    SessionManager.setIntData(SessionManager.language, value!.id);
                    getApiData();
                    Utilities.showSnackBar(context, "Language change successfully");
                    Navigation.canPop(context);
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  allFields() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      CarouselSlider(
        options: CarouselOptions(
            height: MediaQuery.of(context).size.height - 100,
            initialPage: 0,
            viewportFraction: 1.0,
            enlargeCenterPage: false,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            autoPlay: true,
            aspectRatio: 2.0,
            onPageChanged: (index, reason) {
              setState(() {
                _current = index;
              });
            }),
        items: sliderList
            .map((item) => Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    CachedNetworkImage(
                      imageUrl: IMAGE_BASE_URL + item.slider_img_url.toString(),
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      progressIndicatorBuilder: (context, url, downloadProgress) => Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          child: Center(
                              child: Container(
                            height: 150,
                            width: 150,
                            decoration: BoxDecoration(color: Colors.transparent),
                            child: Center(
                                child: CupertinoActivityIndicator(
                              radius: 15,
                            )),
                          ))
                          // child: Center(child: CircularProgressIndicator(value: downloadProgress.progress)),
                          ),
                      errorWidget: (context, url, error) => Icon(Icons.error, size: 50),
                    ),
                    DelayedDisplay(
                        delay: Duration(milliseconds: 1000),
                        child: Text(
                            lanAr
                                ? item.slider_desc_ar.toString()
                                : item.slider_desc.toString(),
                            textAlign: TextAlign.center,
                            textDirection: lanAr ? TextDirection.rtl : TextDirection.ltr,
                            style: Utilities.setTextStyle(
                                AppFontWeight.superBigHeader, AppFontWeight.semiBold,
                                color: AppColor.colorPrimary)))
                  ],
                ))
            .toList(),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: sliderList.asMap().entries.map((entry) {
          return GestureDetector(
            onTap: () => _slideController.animateToPage(entry.key),
            child: Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: (Theme.of(context).brightness == Brightness.dark
                          ? Colors.white
                          : Colors.black)
                      .withOpacity(_current == entry.key ? 0.9 : 0.4)),
            ),
          );
        }).toList(),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Column(
                children: [
                  DelayedDisplay(
                    delay: Duration(milliseconds: 1500),
                    child: Text(
                        lanAr ? AppStrings.featureProductAr : AppStrings.featureProduct,
                        style: Utilities.setTextStyle(
                            AppFontWeight.Header, AppFontWeight.semiBold)),
                  ),
                ],
              ),
            ),

            // With predefined options //Animation ListView Category
            LiveList.options(
              options: options,
              itemCount: categoryList.length,
              shrinkWrap: true,
              controller: ScrollController(keepScrollOffset: false),
              itemBuilder: (context, index, Animation<double> animation) =>
                  FadeTransition(
                opacity: Tween<double>(begin: 0, end: 1).animate(animation),
                child: SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(0, -0.1),
                    end: Offset.zero,
                  ).animate(animation),
                  child: categoryView(index),
                ),
              ),
              scrollDirection: Axis.vertical,
            ),
            SizedBox(height: 25),
            latestPro(),
          ],
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          height: 130,
          width: MediaQuery.of(context).size.width,
          color: AppColor.colorGray,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('If you have any requests, Please feel free to'),
              SizedBox(height: 10),
              InkWell(
                onTap: () => Navigation.push(context, ContactUs()),
                child: Text(
                  'ContactUS',
                  style: Utilities.setTextStyle(
                      AppFontWeight.titleText, AppFontWeight.regular,
                      color: AppColor.colorStarRatings),
                ),
              )
            ],
          ))
    ]);
  }

  categoryView(int index) {
    return InkWell(
      onTap: () {
        setState(() {
          _isLoading = true;
          Navigation.push(
              context,
              SubCategory(
                itemId: categoryList[index].id.toString(),
                categoryName: lanAr
                    ? categoryList[index].categoryNameArabic.toString()
                    : categoryList[index].categoryName.toString(),
              ));
          _isLoading = false;
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: Stack(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(7),
                child: CachedNetworkImage(
                  imageUrl: IMAGE_BASE_URL + categoryList[index].imageUrl.toString(),
                  fit: BoxFit.fill,
                  height: MediaQuery.of(context).size.height / 3,
                  width: MediaQuery.of(context).size.width,
                  progressIndicatorBuilder: (context, url, downloadProgress) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 90),
                      child: Center(
                          child: Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Center(child: CupertinoActivityIndicator()),
                      ))),
                  errorWidget: (context, url, error) => Icon(Icons.error, size: 50),
                )),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 115),
              child: Text(
                  lanAr
                      ? categoryList[index].categoryNameArabic.toString()
                      : categoryList[index].categoryName.toString(),
                  style: Utilities.setTextStyle(
                      AppFontWeight.Header, AppFontWeight.semiBold,
                      color: AppColor.colorPrimary)),
            ),
          ],
        ),
      ),
    );
  }

  latestPro() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Center(
          child: Column(
            children: [
              Text(lanAr ? AppStrings.latestProjectAr : AppStrings.latestProject,
                  style: Utilities.setTextStyle(
                      AppFontWeight.Header, AppFontWeight.semiBold)),
            ],
          ),
        ),
        SizedBox(height: 20),
        tabGridView(),
      ],
    );
  }

  tabGridView() {
    return Stack(
      alignment: Alignment.center,
      children: [
        CarouselSlider(
          carouselController: _controller,
          options: CarouselOptions(
              height: MediaQuery.of(context).size.height / 3,
              initialPage: 0,
              viewportFraction: 1.0,
              enlargeCenterPage: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
          items: allProject
              .map((item) => Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigation.push(context, ProjectDetails(list: item));
                        },
                        child: Container(
                          child: Center(
                              child: CachedNetworkImage(
                            imageUrl: IMAGE_BASE_URL + item.projImgUrl.toString(),
                            fit: BoxFit.fill,
                            width: MediaQuery.of(context).size.width,
                            height: 300,
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error, size: 50),
                          )),
                        ),
                      ),
                      Text(
                        lanAr ? item.projDescAr.toString() : item.projName.toString(),
                        style: Utilities.setTextStyle(
                            AppFontWeight.titleSubHeader, AppFontWeight.semiBold,
                            color: AppColor.colorGray),
                      )
                    ],
                  ))
              .toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              color: AppColor.colorPrimary,
              height: 42,
              width: 35,
              child: IconButton(
                  onPressed: () => _controller.previousPage(),
                  icon: Icon(Icons.arrow_back_ios_new_rounded,
                      size: 26, color: AppColor.colorBlack)),
            ),
            Container(
              color: AppColor.colorPrimary,
              height: 42,
              width: 35,
              child: IconButton(
                onPressed: () => _controller.nextPage(),
                icon: Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 26,
                  color: AppColor.colorBlack,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  @override
  void onConnectionError(String error, String requestCode) {
    Utilities.showSnackBar(context, error);
    print("connection Error:  $error");
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if (value) {
        getApiData();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    if (requestCode == RequestCode.getUser) {
      print("MY error "+ errorMsg);
    }
    print("Api Error:  $errorMsg");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    print("Success: $strMsg");

    if (requestCode == RequestCode.SLIDER) {
      setState(() {
        sliderList =
            ((object as List).map((data) => MainCategories.fromJson(data)).toList());
        // _isLoading = false;
      });
    }

    if (requestCode == RequestCode.PARENT_CATEGORY) {
      setState(() {
        categoryList =
            ((object as List).map((data) => MainCategories.fromJson(data)).toList());
        _isLoading = false;
      });
    }

    if (requestCode == RequestCode.ALL_PROJECT) {
      setState(() {
        allProject =
            (object as List).map((data) => ProjectProducts.fromJson(data)).toList();
        _isLoading = false;
      });
    }

    if (requestCode == RequestCode.CONTACT_US_FOOTER) {
      setState(() {
        contactUsList = ((object as List)
            .map((contactData) => ContactUsModel.fromJson(contactData))
            .toList());
        _isLoading = false;
      });
    }

    if (requestCode == RequestCode.getUser) {
      setState(() {

        print("this one "+ object[0].toString());
        SessionManager.setIntData(SessionManager.USER_ID, object[0]['id']);
        SessionManager.setStringData(SessionManager.USER_EMAIL, object[0]['email'].toString());
        SessionManager.setStringData(SessionManager.USER_NAME, object[0]['name'].toString());
        _isLoading = false;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }
}
