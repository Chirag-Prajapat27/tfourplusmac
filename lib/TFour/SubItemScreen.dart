import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfour/Model/SubItemCategory.dart';
import 'package:tfour/TFour/ProductOverview.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppGif.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/CustomAppBar.dart';
import 'package:tfour/resources/Loader.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';

class SubItemsScreen extends StatefulWidget {
  const SubItemsScreen({Key? key, required this.itemName, required this.id})
      : super(key: key);

  final String itemName;
  final int id;

  @override
  _ItemsScreenState createState() => _ItemsScreenState();
}

class _ItemsScreenState extends State<SubItemsScreen> implements ApiCallBacks {
  List<SubItemCategory> categoryList = [];
  bool _isLoading = false;
  bool lanAr = false;

  @override
  void initState() {
    super.initState();
    categoryAdd();
  }

  void categoryAdd() async {
    setState(() {
      _isLoading = true;
    });
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ? true : false;
    ApiPresenter(this).getProjectProductDetail(widget.id, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: Stack(
        children: [
          SafeArea(
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomeAppBar(
                    onTabMenu: () => Navigation.pop(context),
                    title: widget.itemName),
                Expanded(
                  child: SingleChildScrollView(
                    child: categoryList.isEmpty
                        ? Center(
                            child: Image.asset(AppGif.noDataFoundGIF,
                                filterQuality: FilterQuality.medium,
                                width: MediaQuery.of(context).size.width / 1.1,
                                height: MediaQuery.of(context).size.height),
                          )
                        : allFields(),
                  ),
                ),
              ],
            ),
          ),
          _isLoading ? Loader() : Container()
        ],
      ),
    );
  }

  allFields() {
    return SingleChildScrollView(
      child: GridView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 15),
        controller: ScrollController(keepScrollOffset: false),
        physics: NeverScrollableScrollPhysics(),
        itemCount: categoryList.length,
        itemBuilder: (context, index) {
          return categoryView(index);
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 8,
            mainAxisSpacing: 5,
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height / 1.4)),
      ),
    );
  }

  categoryView(int index) {
    return InkWell(
      onTap: () {
        Navigation.push(context, ProductOverview(list: categoryList[index]));
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            border: Border.all(color: Colors.black, width: 1)),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CachedNetworkImage(
                  height: MediaQuery.of(context).size.height / 5,
                  imageUrl:
                      IMAGE_BASE_URL + categoryList[index].proImgUrl.toString(),
                  fit: BoxFit.fill,
                  width: MediaQuery.of(context).size.width,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      Center(
                          child: Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Center(child: CupertinoActivityIndicator()),
                      )),
                  errorWidget: (context, url, error) => Icon(Icons.error)),
              SizedBox(height: 12),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        // "${categoryList[index].id} " +  //This is comment for not show category ID
                        "" +
                            (lanAr
                                ? categoryList[index].proNameAr.toString()
                                : categoryList[index].proName.toString()),
                        // softWrap: false,
                        overflow: TextOverflow.clip,
                        style: Utilities.setTextStyle(
                            AppFontWeight.subTitleText, AppFontWeight.semiBold,
                            color: AppColor.colorBlack)),
                    SizedBox(height: 2),
                    Text(
                        lanAr
                            ? categoryList[index].proDescAr.toString()
                            : categoryList[index].proDesc.toString(),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Utilities.setTextStyle(
                            AppFontWeight.subTitleText, AppFontWeight.regular,
                            color: AppColor.colorBlack))
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onConnectionError(String error, String requestCode) {
    Utilities.showSnackBar(context, error);
    setState(() {
      _isLoading = false;
    });
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => NoInternetConnection(requestCode: requestCode)),
    // ).then((value) {
    //   if(value){
    //     categoryAdd();
    //   }
    // });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    print("Error $errorMsg");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) {
    if (requestCode == RequestCode.PRODUCT_DETAIL) {
      setState(() {
        categoryList.addAll(
            (object as List).map((e) => SubItemCategory.fromJson(e)).toList());
        _isLoading = false;
      });
    }
  }
}
