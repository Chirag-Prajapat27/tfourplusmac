import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfour/TFour/Dashboard.dart';
import 'package:tfour/TFour/SignUp.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/resources/CustomButton.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/Utilities.dart';
import '../api/RequestCode.dart';
import '../module/ApiPresenter.dart';
import '../resources/CustomTextField.dart';
import '../resources/Loader.dart';
import '../resources/NoInternetConnection.dart';
import '../utility/SessionManager.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> implements ApiCallBacks {
  TextEditingController tfEmailAddress = TextEditingController();
  TextEditingController tfPassword = TextEditingController();
  List<String> errorMessageList = ['', ''];
  bool _isLoading = false;
  bool isShowPass = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorPrimary,
      body: SafeArea(
          child:  _isLoading ? Loader() : Padding(
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 40),
                  Image.asset(AppImages.logoTFour),
                  SizedBox(height: 20),
                  Text(
                    "Log In",
                    style: Utilities.setTextStyle(
                        AppFontWeight.Header, AppFontWeight.regular,
                        color: AppColor.colorBlack),
                  ),
                  SizedBox(height: 30),
                  CustomTextField(
                      hintText: AppStrings.enterEmail,
                      controller: tfEmailAddress,
                      autofocus: true,
                      errorMessage: errorMessageList[0],
                      textInputType: TextInputType.emailAddress,
                      borderRadius: 20,
                      onTextChange: () {}),
                  SizedBox(height: 10),
                  CustomTextField(
                      hintText: AppStrings.enterPassword,
                      controller: tfPassword,
                      obscureText: isShowPass,
                      errorMessage: errorMessageList[1],
                      textInputType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.done,
                      borderRadius: 20,
                      isShowEye: true,
                      onTapEye: () {
                        setState(() {
                          isShowPass = !isShowPass;
                        });
                      },
                      onTextChange: () {}),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
                    child: CustomButton(
                      borderRadius: 40,
                      buttonTitle: AppStrings.submitCap,
                      boxShadowColor: AppColor.colorGray,
                      blurRadius: 8,
                      onTapButton: () {
                        if (formValidation()) {
                          getApiData();
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25),
                    child: InkWell(
                      onTap: () {
                        Navigation.pushAndRemoveUntil(context, SignUp());
                      },
                      child: Text(
                        AppStrings.signUp,
                        style: Utilities.setTextStyle(
                            AppFontWeight.titleSubHeader, AppFontWeight.medium),
                      ),
                    ),
                  ),
                  SizedBox(height: 10)
                ],
              ),
            ),
          )),
    );
  }

  bool formValidation() {
    bool isValid = true;
    setState(() {
      for (int v = 0; v < errorMessageList.length; v++) {
        if (v == 0) {
          if (tfEmailAddress.text.toString().isEmpty) {
            errorMessageList[0] = AppStrings.emptyEmail;
            isValid = false;
          } else {
            if (!Utilities.validateEmail(tfEmailAddress.text.toString().trim())) {
              errorMessageList[0] = AppStrings.validEmail;
              isValid = false;
            } else
              errorMessageList[0] = '';
          }
        } else if (v == 1) {
          if (tfPassword.text.toString().trim().isEmpty) {
            errorMessageList[1] = AppStrings.emptyPassword;
            isValid = false;
          } else
            errorMessageList[1] = '';
        }
      }
    });
    return isValid;
  }

  /*Call Login Api*/
  getApiData() async {
    setState(() {
      _isLoading = true;
    });
    ApiPresenter(this).signIn(
        email: tfEmailAddress.text.trim().toString(),
        password: tfPassword.text.trim().toString());
  }

  @override
  void onConnectionError(String error, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if (value) {
        initState();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    setState(() {
      _isLoading = false;
    });
    if (requestCode == RequestCode.SIGN_IN) {
      setState(() {
        Utilities.showSnackBar(context, errorMsg);
        _isLoading = false;
      });
    }
  }

  @override
  void onSuccess(object, String strMsg, String requestCode) async {
    if (requestCode == RequestCode.SIGN_IN) {
        String token = object['token'];
        SessionManager.setStringData(SessionManager.ACCESS_TOKEN, token);
        print("Token session :: $token");

      setState(() {
        Utilities.showSnackBar(context, strMsg);
        Navigation.pushAndRemoveUntil(context, Dashboard());
        _isLoading = false;
      });
    }
  }
}
