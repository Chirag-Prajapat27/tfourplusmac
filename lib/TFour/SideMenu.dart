import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:tfour/Model/MainCategories.dart';
import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppImages.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:tfour/module/ApiPresenter.dart';
import 'package:tfour/resources/NoInternetConnection.dart';
import 'package:tfour/utility/ScreenNavigation.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../Model/MenuItem.dart';
import 'Dashboard.dart';
import 'Login.dart';
import 'SideMenuScreensUI/AboutUs.dart';
import 'SideMenuScreensUI/ContactUs.dart';
import 'SideMenuScreensUI/ProjectNew.dart';
import 'SubCategory.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> implements ApiCallBacks {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<MenuItem> menuList = [];
  bool _isLoading = false;
  List<MainCategories> categoryList = [];
  bool lanAr = false;

  @override
  void initState() {
    super.initState();
    // addMenuItems();
    getApiData();
  }

  getApiData() async {
    setState(() {
      _isLoading = true;
    });
    int lanValue = await SessionManager.getIntData(SessionManager.language);
    lanAr = lanValue != 1 ? true : false;
    ApiPresenter(this).getParentCategory(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        key: _scaffoldKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              DrawerHeader(
                  padding: EdgeInsets.all(0),
                  child: Image.asset(AppImages.logoTFour,
                    // fit: BoxFit.cover,
                    height: 200,
                    width: 200,)),
              DrawerListTile(
                title: lanAr ? AppStrings.homeAr : AppStrings.home,
                icon: Icons.home,
                press: () {
                  Navigation.pop(context);
                  Navigation.push(context, Dashboard());
                },
              ),
              DrawerListTile(
                title: lanAr ? AppStrings.warehouseAr : AppStrings.warehouse,
                icon: Icons.warehouse,
                press: () {
                  Navigation.pop(context);
                },
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                height: 40,
                child: Row(
                  children: [
                    Icon(Icons.shopping_bag,size: 30),
                    SizedBox(width: 15),
                    Center(
                      child: DropdownButton2(
                        underline: Container(),
                        hint: Text(
                          lanAr ? AppStrings.productsAr : AppStrings.products,
                          style: Utilities.setTextStyle(
                              AppFontWeight.titleText, AppFontWeight.semiBold),
                        ),
                        items: categoryList.map(
                              (item) => DropdownMenuItem<MainCategories>(
                                value: item, // this value show on tags
                                child: Text(
                                  lanAr
                                      ? item.categoryNameArabic.toString()
                                      : item.categoryName.toString(),
                                  textDirection: lanAr
                                      ? TextDirection.rtl
                                      : TextDirection.ltr,
                                ),
                              ),
                            ).toList(),
                        onChanged: (MainCategories? value) {
                          Navigation.pop(context);
                          Navigation.push(
                            context,
                            SubCategory(
                              itemId: value!.id.toString(),
                              categoryName: lanAr
                                  ? value.categoryNameArabic.toString()
                                  : value.categoryName.toString(),
                            ),
                          );
                        },
                        dropdownWidth: 220,
                        dropdownDecoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              DrawerListTile(
                title: lanAr ? AppStrings.projectAr : AppStrings.project,
                icon: Icons.business,
                press: () {
                  Navigation.pop(context);
                  Navigation.push(context, ProjectNew());
                  // Navigation.push(context, Projects());
                },
              ),
              DrawerListTile(
                title: lanAr ? AppStrings.contactUsAr : AppStrings.contactUs,
                icon: Icons.contact_page_rounded,
                press: () {
                  Navigation.pop(context);
                  Navigation.push(context, ContactUs());
                },
              ),
              DrawerListTile(
                title: lanAr ? AppStrings.aboutUsAr : AppStrings.aboutUs,
                icon: Icons.groups,
                press: () {
                  Navigation.pop(context);
                  Navigation.push(context, AboutUs());
                },
              ),
              DrawerListTile(
                title: lanAr ? AppStrings.logoutAr : AppStrings.logout,
                icon: Icons.logout_rounded,
                press: () {
                  Utilities.showDialogCommonBoth(
                      context,
                      lanAr ? AppStrings.logoutAr : AppStrings.logoutDialogTitle,
                      lanAr ? AppStrings.logoutAr : AppStrings.logout,
                      lanAr ? AppStrings.cancelAr : AppStrings.cancel,
                      AppColor.colorRed,
                      onYes: () {
                        Navigation.pop(context);
                            SessionManager.clearSession().then((value) =>
                                Navigation.pushAndRemoveUntil(context, Login()));
                      },
                    onCancel: () {
                      Navigation.pop(context);
                    }
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onConnectionError(String error, String requestCode) {
    Utilities.showSnackBar(context, error);
    setState(() {
      _isLoading = false;
    });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NoInternetConnection(requestCode: requestCode)),
    ).then((value) {
      if (value) {
        getApiData();
      }
    });
  }

  @override
  void onError(String errorMsg, String requestCode) {
    print("Error $errorMsg");
    setState(() {
      _isLoading = false;
    });
  }


  @override
  void onSuccess(object, String strMsg, String requestCode) {
    if (requestCode == RequestCode.PARENT_CATEGORY) {
      setState(() {
        categoryList.addAll((object as List)
            .map((data) => MainCategories.fromJson(data))
            .toList());
        _isLoading = false;
      });
    }
  }
}

class DrawerListTile extends StatelessWidget {
  DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.press,
    this.svgSrc,
    this.icon,
    this.isIcon = true,
  }) : super(key: key);

  final String? title, svgSrc;
  final VoidCallback press;
  bool isIcon = true;
  final IconData? icon;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 5,
      leading: isIcon ? Icon(icon, color: AppColor.colorBlack, size: 30) : SvgPicture
          .asset(
        svgSrc!,
        color: Colors.white54,
        height: 16,
      ),
      title: Text(title!,
        style: Utilities.setTextStyle(
            AppFontWeight.titleText, AppFontWeight.semiBold),
      ),
    );
  }
}
