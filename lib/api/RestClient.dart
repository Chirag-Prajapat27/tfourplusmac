import 'dart:convert';
import 'dart:io';
import 'package:tfour/utility/SessionManager.dart';
import 'package:tfour/utility/Utilities.dart';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'ApiInterface.dart';
import 'WebFields.dart';
import 'WebUrl.dart';

class RestClient {

  ApiCallBacks _apiCallBacks;
  RestClient(this._apiCallBacks);

  /* Call api using http request
  * @param context
  * @param requestType - GET,POST,DELETE...
  * @param requestParams - Pass api request parameters
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  Future<dynamic> post(String requestCode, Map requestParam ) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError(
          "Check your internet connection and try again", requestCode);
    } else {
      try {
        String url = WebUrl.MAIN_URL + requestCode;
        print(
            "Request Param :" + url.toString() + " " + requestParam.toString());
        String assessToken = await SessionManager.getStringData(SessionManager.ACCESS_TOKEN);
        final response = await http
            .post(Uri.parse(url),
                headers: {
                  HttpHeaders.contentTypeHeader: 'application/json',
                  HttpHeaders.authorizationHeader: 'Bearer ' + assessToken
                },
                body: json.encode(requestParam)).timeout(Duration(seconds: 50));
        print("Token RESTClient POST :: $assessToken");

        final responseBody = json.decode(response.body);
        print("responseBody: $responseBody");     // RESPONSE BODY PRINTED TO SEE DATA.

        if (response.statusCode != 200 && responseBody == null) {
          print("status == false"); //400 = false and 200 = true
          _apiCallBacks.onError(
              "An error occurred Status Code : $response.statusCode", requestCode);
        } else {
          if (responseBody["status"]) {
            _apiCallBacks.onSuccess(responseBody['data'], responseBody['message'], requestCode);
          } else {
            _apiCallBacks.onError(responseBody['message'], requestCode);
          }
        }
      } catch (e) {
        print(e);
        _apiCallBacks.onError("something went wrong try again...", requestCode);
      }
    }
  }

  //For PUT type API
  /* Call api using http request
  * @param context
  * @param requestType - GET,POST,DELETE...
  * @param requestParams - Pass api request parameters
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  Future<dynamic> put(Map requestParam, String requestCode) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError(
          "Check your internet connection and try again", requestCode);
    } else {
      try {
        String url = WebUrl.MAIN_URL + requestCode;
        print(
            "Request Param :" + url.toString() + " " + requestParam.toString());
        String assessToken = await SessionManager.getStringData(SessionManager.ACCESS_TOKEN);
        final response = await http
            .put(Uri.parse(url),
            headers: {
              HttpHeaders.contentTypeHeader: 'application/json',
              HttpHeaders.authorizationHeader: 'Bearer ' + assessToken
            },
            body: json.encode(requestParam))
            .timeout(Duration(seconds: 50));
        print("Token RESTClient Put :: $assessToken");

        // print("url: " + response.body.toString());

        final responseBody = json.decode(response.body);
        print("responseBody: $responseBody");     // RESPONSE BODY PRINTED TO SEE DATA.

        if (response.statusCode != 200 && responseBody == null) {
          print("status == false"); //400 = false and 200 = true
          _apiCallBacks.onError(
              "An error occurred Status Code : $response.statusCode", requestCode);
        } else {
          if (responseBody["status"]) {
            _apiCallBacks.onSuccess(responseBody['data'], responseBody['message'], requestCode);
          } else {
            _apiCallBacks.onError(responseBody['message'], requestCode);
          }
        }
      } catch (e) {
        print(e);
        _apiCallBacks.onError(e.toString(), requestCode);
      }
    }
  }

  /* Call api using http request
  * @param context
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  Future<dynamic> get(String requestCode,{String queryParams='', String assessToken = ''}) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError(
          "Check your internet connection and try again", requestCode);
    } else {
      try {
        String url = WebUrl.MAIN_URL + requestCode;
        if(queryParams.isNotEmpty){
          url +=queryParams;
        }
        print("Request Param :" + url.toString());
        final response = await http.get(Uri.parse(url), headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          if (assessToken.isNotEmpty) HttpHeaders.authorizationHeader: 'Bearer ' + assessToken
        }).timeout(Duration(seconds: 30));
        final responseBody = json.decode(response.body);
        if (response.statusCode != 200 && responseBody == null) {
          _apiCallBacks.onError(
              "An error occurred Status Code : $response.statusCode",
              requestCode);
        } else {
          if (responseBody["status"]) {
            _apiCallBacks.onSuccess(
                responseBody['data'], responseBody['message'], requestCode);
          } else {
            print("status == ${response.statusCode}");
            _apiCallBacks.onError(responseBody["message"], requestCode);
          }
        }
      } catch (e) {
        print(e.toString());
         _apiCallBacks.onError(e.toString(), requestCode);
      }
    }
  }


  //http Delete API call
  Future<dynamic> delete(String requestCode,{String queryParams=''}) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError(
          "Check your internet connection and try again", requestCode);
    } else {
      try {
        String url = WebUrl.MAIN_URL + requestCode;
        if(queryParams.isNotEmpty){
          url +=queryParams;
        }
        print("Request Param :" + url.toString());
        final response = await http.delete(Uri.parse(url), headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        }).timeout(Duration(seconds: 20));
        final responseBody = json.decode(response.body);
        if (response.statusCode != 200 && responseBody == null) {
          _apiCallBacks.onError(
              "An error occurred Status Code : $response.statusCode",
              requestCode);
        } else {
          if (responseBody["status"]) {
            _apiCallBacks.onSuccess(
                responseBody['data'], responseBody['message'], requestCode);
          } else {
            print("status == ${response.statusCode}");
            _apiCallBacks.onError(responseBody["message"], requestCode);
          }
        }
      } catch (e) {
        _apiCallBacks.onError("something went wrong try again...", requestCode);
      }
    }
  }



  /* Upload image on server using http request
  * @param context
  * @param requestParams - Pass api request parameters
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  void uploadImage(File profileImage,String imageName,String requestCode,{String documentType = '',String userId=''}) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError("Internet is not connected", requestCode);
    } else {
      String url = WebUrl.MAIN_URL + requestCode;
      print('$url');
      var uri = Uri.parse(url);
      var request = new http.MultipartRequest("POST", uri);

      // Map<String, String> headers = {HttpHeaders.contentTypeHeader: 'application/json'};
      Map<String, String> headers = {};

      request.headers.addAll(headers);
      if(documentType.isNotEmpty)
        {
          request.fields['type']= documentType;
        }
      if(userId.isNotEmpty)
      {
        request.fields['userId']= userId;
      }
      if (profileImage != null) {
        var stream = new http.ByteStream(
            DelegatingStream.typed(profileImage.openRead()));
        var length = await profileImage.length();
        var multipartFile = new http.MultipartFile(imageName, stream, length,
            filename: basename(profileImage.path));
        request.files.add(multipartFile);
      }
      try {
        var response = await request.send().timeout(Duration(seconds: 60));
        print(response.statusCode);
        response.stream.transform(utf8.decoder).listen((value) {
          final responseBody = json.decode(value);
          if (response.statusCode != 200 && responseBody == null) {
            _apiCallBacks.onError(
                "An error occurred Status Code : $response.success",
                requestCode);
          } else {
            if (responseBody["status"]) {
              _apiCallBacks.onSuccess(
                  responseBody['data'], responseBody['message'], requestCode);
            } else {
              print("status == false");
              _apiCallBacks.onError(responseBody["message"], requestCode);
            }
          }
        });
      } on Exception catch (e) {
        _apiCallBacks.onError(e.toString(), requestCode);
      }
    }
  }



  /* Upload image on server using http request
  * @param context
  * @param requestParams - Pass api request parameters
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  void uploadMultipleImages(List<File> imageList, String moduleName, String type,
      String imageName, bool isMultimedia, String requestCode) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError("Internet is not connected", requestCode);
    } else {
      String url = WebUrl.MAIN_URL + requestCode;
      var uri = Uri.parse(url);
      var request = new http.MultipartRequest("POST", uri);
      if (isMultimedia) {
        request.fields['mediaType'] = type;
        request.fields['moduleName'] = moduleName;
      } else {
        request.fields['moduleName'] = moduleName;
      }
      Map<String, String> headers = {
        "authorization":
        'Bearer ' + await SessionManager.getStringData(SessionManager.ACCESS_TOKEN)
      };
      request.headers.addAll(headers);
      if (imageList.isNotEmpty) {
        for(int i = 0;i<imageList.length;i++)
          {
            var stream = new http.ByteStream(
                DelegatingStream.typed(imageList[i].openRead()));
            var length = await imageList[i].length();
            var multipartFile = new http.MultipartFile(imageName, stream, length,
                filename: basename(imageList[i].path));
            request.files.add(multipartFile);
          }
      }
      try {
        var response = await request.send().timeout(Duration(seconds: 60));
        print(response.statusCode);
        response.stream.transform(utf8.decoder).listen((value) {
          final responseBody = json.decode(value);
          if (response.statusCode != 200 && responseBody == null) {
            _apiCallBacks.onError(
                "An error occurred Status Code : $response.success",
                requestCode);
          } else {
            if (responseBody["status"]) {
              _apiCallBacks.onSuccess(
                  responseBody['data'], responseBody['message'], requestCode);
            } else {
              print("status == false");
              _apiCallBacks.onError(responseBody["message"], requestCode);
            }
          }
        });
      } on Exception catch (e) {
        _apiCallBacks.onError(e.toString(), requestCode);
      }
    }
  }

  /* Get google search service API data
  * @param context
  * @param requestParams - Pass api request parameters
  * @param requestCode - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */
  Future<dynamic> getGooglePlaces(String keyword) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (!isInternet) {
      _apiCallBacks.onConnectionError("Internet is not connected", "Google");
    } else {
      try {
        String url =  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key="+WebFields.kGoogleApiKey+"&input="+keyword+"&sensor=true";
        print("Request Param :" + url);
        final response = await http.get(Uri.parse(url),headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        }).timeout(Duration(seconds: 10));
        final responseBody = json.decode(response.body);
        if (response.statusCode != 200 && responseBody == null) {
          _apiCallBacks.onError(
              "An error occurred Status Code : $response.statusCode", "Google");
        } else {
          if (responseBody["status"] == "OK") {
            // var returnObject =
            // ResponseManager.parser("Google", responseBody);
            _apiCallBacks.onSuccess(
                responseBody['predictions'], responseBody['msg'], "Google");
            // _apiCallBacks.onSuccess(returnObject, responseBody['msg'], "Google");
          } else {
            print("status == 0");
            _apiCallBacks.onError(responseBody["msg"], "Google");
          }
        }
      } catch (e) {
        _apiCallBacks.onError(e.toString(), "Google");
      }
    }
  }
}
