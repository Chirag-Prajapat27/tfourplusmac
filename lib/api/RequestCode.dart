class RequestCode {

  static const String PARENT_CATEGORY ='ParentCategory/getData.php';

  static const String SUB_CATEGORY ='SubCategory/data.php';

  static const String ALL_PROJECT ='LatestProject/allProjects.php';
  static const String PROJECT_CATEGORIES_NAME ='LatestProject/projectCategories.php';

  static const String PRODUCT_DETAIL ='ProductDetail/getData.php';

  static const String SLIDER ='Slider/getData.php';

  static const String ABOUT_US='AboutUs/getData.php';
  static const String ABOUT_US_TEAM ='AboutUs/getTeam.php';

  static const String CONTACT_US_FOOTER ='ContactUs/getFooter.php';
  static const String CONTACT_FORM ='ContactUs/contactUsForm.php';

  static const String SIGN_IN ='Signin/login.php';
  static const String SIGN_UP ='Signup/register.php';
  static const String getUser ='GetUser/getUser.php';

}
