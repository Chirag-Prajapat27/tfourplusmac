import 'package:tfour/api/ApiInterface.dart';
import 'package:tfour/api/RequestCode.dart';
import 'package:tfour/api/RestClient.dart';
import 'package:tfour/api/WebFields.dart';
import 'package:flutter/material.dart';

class ApiPresenter {
  ApiCallBacks _apiCallBacks;

  ApiPresenter(this._apiCallBacks);

  // for Slider data
  getSlider(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.SLIDER);
  }

  // for About Us  data
  getAboutUs(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.ABOUT_US);
  }

  // for About Us  data
  getTeam(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.ABOUT_US_TEAM);
  }

  // for Parent categories
  getParentCategory(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.PARENT_CATEGORY);
  }

  // for All Project Categories Details
  getAllProject(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.ALL_PROJECT);
  }

  // for Project Categories Name
  getProjectCategoriesName(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.PROJECT_CATEGORIES_NAME);
  }

  // for Contact US details
  getContactUsInfo(BuildContext context) async {
    RestClient(_apiCallBacks).get(RequestCode.CONTACT_US_FOOTER);
  }

  //FOR getUser using put method
  getUser(BuildContext context, String accessToken) async {
    RestClient(_apiCallBacks).get(RequestCode.getUser, assessToken: accessToken);
  }

  // get Sub Category
  getSubCategoryItems(int parentCategoryId, BuildContext context) async {
    String requestParam = "?id=$parentCategoryId";
    RestClient(_apiCallBacks)
        .get(RequestCode.SUB_CATEGORY, queryParams: requestParam);
  }

  // get Project Category Details
  getProjectProductDetail(int projectCategoryId, BuildContext context) async {
    String requestParam = "?id=$projectCategoryId";
    RestClient(_apiCallBacks)
        .get(RequestCode.PRODUCT_DETAIL, queryParams: requestParam);
  }

  //FOR Contact Us Form
  contactForm(String mobileNO,String email,String message, BuildContext context) async {
    Map requestParam = toCheckMobileNoJson(mobileNO,email,message);
    RestClient(_apiCallBacks).post(RequestCode.CONTACT_FORM, requestParam);
  }

  Map<String, dynamic> toCheckMobileNoJson(String mobileNO,String email,String message) => {
        WebFields.NAME: mobileNO,
        WebFields.EMAIL: email,
        WebFields.MESSAGE: message,
      };

  //FOR Register user
  signUp(
      {required String name,
      required String email,
      required String password,
      required String phoneType}) async {
    Map requestParam =
    toRegister(name, email, password, phoneType);
    RestClient(_apiCallBacks).post(
        RequestCode.SIGN_UP, requestParam); //END POINT in RequestCode
  }

  Map<String, dynamic> toRegister(String name, String email,
          String password, String phoneType) =>
      {
        WebFields.NAME: name,
        WebFields.EMAIL: email,
        WebFields.PASSWORD: password,
        WebFields.PHONE_TYPE: phoneType
      };

  //For Login user
  signIn(
      {required String email,
        required String password}) async {
    Map requestParam =
    toLogin(email, password);
    RestClient(_apiCallBacks).post(
        RequestCode.SIGN_IN, requestParam); //END POINT in RequestCode
  }

  Map<String, dynamic> toLogin(String email, String password) =>
      {
        WebFields.EMAIL: email,
        WebFields.PASSWORD: password
      };

  //GET GOOGLE PLACES
  getGooglePlaces(String keyword) async {
    RestClient(_apiCallBacks).getGooglePlaces(keyword);
  }

  // //FOR Upload Image
  // uploadImages(File image, String documentType,String userId, BuildContext context) async {
  //   RestClient(_apiCallBacks).uploadImage(
  //       image, WebFields.UPLOAD_IMAGE, RequestCode.UPLOAD_IMAGE,
  //       documentType: documentType,userId: userId);
  // }



 // for get------------------------------------------

  // // FOR GET ENQUIRY
  // getEnquiry(
  //     int userId, int limit, int page_number, BuildContext context) async {
  //   String queryParams =
  //       "?${WebFields.USERID}=$userId&${WebFields.LIMIT}=$limit&${WebFields.PAGE_NUMBER}=$page_number";
  //   RestClient(_apiCallBacks)
  //       .get(RequestCode.GET_ENQUIRY, queryParams: queryParams);
  // }

  // for post-----------------------------------------

  // FOR RATINGS
  //ADD NEW 3 FIELDS FROM RESPONSE HERE, WHEN IT IS DONE IN API FOR NULL ERROR.
    // giveRatings(
    //     {required String reviewById,
    //     required int reviewToId,
    //     required String ratingNumber,
    //     required String reviewComments}) async {
    //   Map requestParam =
    //       toRatingsJson(reviewById, reviewToId, ratingNumber, reviewComments);
    //   RestClient(_apiCallBacks).post(
    //       requestParam, RequestCode.RATINGS_REVIEWS); //END POINT in RequestCode
    // }
    //
    // Map<String, dynamic> toRatingsJson(String reviewById, int reviewToId,
    //         String ratingNumber, String reviewComments) =>
    //     {
    //       WebFields.REVIEW_BY_ID: reviewById,
    //       WebFields.REVIEW_TO_ID: reviewToId,
    //       WebFields.RATING_NO: ratingNumber,
    //       WebFields.REVIEW_COMMENT: reviewComments,
    //     };

}
