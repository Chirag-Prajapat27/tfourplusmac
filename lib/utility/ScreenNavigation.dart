import 'package:flutter/material.dart';

class Navigation {
  static void push(BuildContext context, Widget route) {
    //for Open New Screen
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => route),
    );
  }

  //for Replace with Current Screen
  static void pushReplacement(BuildContext context, Widget route) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => route),
    );
  }

  //for remove back Screens
  static void pushAndRemoveUntil(BuildContext context, Widget route) {
    Navigator.pushAndRemoveUntil<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => route,
      ),
      (route) => false,
    );
  }

  static void removeRoute(BuildContext context, Route<dynamic> route) {
    return Navigator.of(context).removeRoute(route);
  }

  //for Back Press
  static void pop(BuildContext context) {
    Navigator.of(context).pop();
  }

  static void popWithRoute(BuildContext context, Widget route) {
    Navigator.of(context).pop(route);
  }


  static void popUntil(BuildContext context, RoutePredicate predicate) {
    Navigator.of(context).popUntil(predicate);
  }

  static bool canPop(BuildContext context) {
    final NavigatorState? navigator = Navigator.maybeOf(context);
    return navigator != null && navigator.canPop();
  }
}
