import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  static const String language = 'languageId';
  static const String ACCESS_TOKEN = 'ACCESS_TOKEN';
  static const String USER_ID = 'user_id';
  static const String USER_EMAIL = 'user_name';
  static const String USER_NAME = 'user_name';


  static void setListDynamicData(String key, String val) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(val));
  }

  static Future<String> getListDynamicData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key)!);
  }

  static void   setStringData(String key, String val) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, val);
  }

  static void setUserData(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  static void setStringList(String key, List<String> val) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, val);
  }

  static void setIntData(String key, int val) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, val);
  }

  static void setBooleanData(String key, bool val) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, val);
  }

  static Future<String> getStringData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }

  static Future<String> getUserData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key)!);
  }

  static Future<int> getIntData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key) ?? -1;
  }

  static Future<bool> getBooleanData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key)!;
  }

  static Future<bool?> clearSession() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }
}
