import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tfour/constants/AppColor.dart';
import 'package:tfour/constants/AppFontWeight.dart';
import 'package:tfour/constants/AppString.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'ScreenNavigation.dart';

class Utilities {
  static showSnackBar(BuildContext context, String msg) {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
      content: Container(
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              // Icon(Icons.error, color: Colors.red, size: 30),
              SizedBox(width: 8),
              Flexible(
                  child: Text(msg,
                      style:
                          TextStyle(color: AppColor.colorSystemWhite, fontSize: 14))),
            ],
          ),
        ),
      ),
      duration: Duration(seconds: 2),
      backgroundColor: Colors.black,
    ));
  }

  static showError(GlobalKey<ScaffoldState> scaffoldKey, String msg) {
    ScaffoldMessenger.of(scaffoldKey.currentContext!).showSnackBar(SnackBar(
      content: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.error, color: Colors.red, size: 30),
              SizedBox(width: 8),
              Flexible(
                  child: Text(msg,
                      style:
                          TextStyle(color: AppColor.colorTheme, fontSize: 14))),
            ],
          ),
        ),
      ),
      duration: Duration(seconds: 2),
      backgroundColor: Colors.white,
    ));
  }

  static showSuccessMessage(GlobalKey<ScaffoldState> scaffoldKey, String msg) {
    ScaffoldMessenger.of(scaffoldKey.currentContext!).showSnackBar(SnackBar(
      content: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.check_circle, color: Colors.green, size: 25),
              SizedBox(width: 8),
              Flexible(
                  child: Text(
                msg,
                style: TextStyle(color: AppColor.colorTheme, fontSize: 14),
              )),
            ],
          ),
        ),
      ),
      duration: Duration(seconds: 2),
      backgroundColor: Colors.white,
    ));
  }

  static launchCall(String number) async {
    var url = Uri.parse("tel:$number");
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      print('could not launch');
    }
  }

  static String parseDate(String dateFormat,String date) {

    return DateFormat(dateFormat).format(DateTime.parse(date));
  }

  static launchURL(String toMailId, String subject, String body) async {
    var url = Uri.parse('mailto:$toMailId?subject=$subject&body=$body');
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static launchYouTube(String link) async {
    var url = Uri.parse(link);
    print("Youtube :: $url");
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not open this Video';
    }
  }
  static launchFaceBook(String link) async {
    var url = Uri.parse('fb://facewebmodal/f?href=$link');
    print("Youtube :: $url");
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not open this Video';
    }
  }


  static myWhat() async {

      if (Platform.isAndroid) {
        // add the [https]
        return "https://wa.me/123/?text=${Uri.parse("hello")}"; // new line
      } else {
        // add the [https]
        return "https://api.whatsapp.com/send?phone=123=${Uri.parse("Hello")}"; // new line
      }

  }

  static openWhatsapp(BuildContext context,String whatsAppNO) async{
    var whatsAppURlAndroid = Uri.parse("whatsapp://send?phone=$whatsAppNO&text=hello");

    var whatAppURLIos =Uri.parse("https://wa.me/$whatsAppNO?text=${Uri.parse("hello")}");
    if(Platform.isIOS){
      // for iOS phone only
      if( await canLaunchUrl(whatAppURLIos)){
        await launchUrl(whatAppURLIos);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));
      }
    } else if(Platform.isAndroid){
      if( await canLaunchUrl(whatsAppURlAndroid)){
        await launchUrl(whatsAppURlAndroid);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsApp no installed")));
      }
    }else{
      // android , web
      if( await canLaunchUrl(whatsAppURlAndroid)){
        await launchUrl(whatsAppURlAndroid);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));
      }
    }
  }

  static bool isMobileNumber(String value) {
    RegExp regex = RegExp(r'^-?[0-9]{10}$');
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  //check validation of email
  static bool validateEmail(String value) {
    RegExp regex = new RegExp('^\\S+@\\S+\\.\\S+\$');
        // r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,5}))$');
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  //Check Validation of Mobile No.
  static bool validateMobileNo(String value) {
    RegExp regex = new RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)');
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }


  static showDialogCommon(BuildContext context, String? title,
      String? btnOneText, String? btnTwoText,Color? btnOneColor,{Function? onYes,Function? onCancel}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(title.toString()),
            actions: <Widget>[
              CupertinoDialogAction(
                  onPressed: () {
                    onYes!();
                  },
                  child: Text(btnOneText.toString(),
                      style: TextStyle(color: btnOneColor == null? Colors.lightBlue:btnOneColor))),
              CupertinoDialogAction(
                  onPressed: () {
                    onCancel!();
                  },
                  isDefaultAction: true,
                  child: Text(btnTwoText.toString())),
            ],
          );
        });
  }

  static showDialogCommonBoth<Void>(BuildContext context, String? title, String? btnOneText,
      String? btnTwoText, Color? btnOneColor, {Function? onYes, Function? onCancel}) {

    Utilities.getIsAndroid(context) ?
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title.toString()),
            actions: <Widget>[
              TextButton(
                  child: Text(btnOneText.toString(),
                      style: TextStyle(
                          color: btnOneColor == null ? Colors.lightBlue : btnOneColor)),
                  onPressed: () {
                    Navigation.pop(context);
                    onYes!();
                  }
              ),
              TextButton(
                onPressed: () {
                  Navigation.pop(context);
                  onCancel!();
                },
                child: Text(btnTwoText.toString()),
              ),
            ],
          );
        })  :
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(title.toString()),
            actions: <Widget>[
              CupertinoDialogAction(
                  onPressed: () {
                    Navigation.pop(context);
                    onYes!();
                  },
                  child: Text(btnOneText.toString(),
                      style: TextStyle(color: btnOneColor == null? Colors.lightBlue:btnOneColor))),
              CupertinoDialogAction(
                  onPressed: () {
                    Navigation.pop(context);
                    onCancel!();
                  },
                  isDefaultAction: true,
                  child: Text(btnTwoText.toString())),
            ],
          );
        });
  }

  //Check Validation of Name.
  static bool validateName(String value) {
    RegExp regex = new RegExp(r'(^(?:[A-Z]1)?[A-Za-z]{3,15}$)');
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  /* get device type
  * @param context
  * */
  // static String getDeviceType(BuildContext context) {
  //   return Theme.of(context).platform == TargetPlatform.iOS ? "iOS" : "Android";
  // }

  static String getDeviceType(BuildContext context) {
    if (Platform.isIOS){
      return "iOS";
    } else if (Platform.isAndroid){
      return "Android";
    } else {
      return "Web";
    }
  }

  static bool getIsAndroid(BuildContext context) {
    if (Platform.isAndroid)
      return true;
    else
      return true;
  }



  // static Future<String> getDeviceToken(BuildContext context) async {
  //   final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  //   if (Theme.of(context).platform == TargetPlatform.android) {
  //     return await deviceInfoPlugin.androidInfo
  //         .then((deviceInfo) => deviceInfo.androidId.toString());
  //   } else if (Theme.of(context).platform == TargetPlatform.iOS) {
  //     return await deviceInfoPlugin.iosInfo
  //         .then((ss) => ss.identifierForVendor.toString());
  //   } else {
  //     return "";
  //   }
  // }

  static List<String> getSplitSpace(String str) {
    return str.split(" ").toList();
  }

  static Future<bool> isConnectedNetwork() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet true");
        return true;
      } else {
        print("Internet false");
        return false;
      }
    } on SocketException catch (_) {
      print("Internet false catch");
      return false;
    }
  }

  static TextStyle setTextStyle(double fontSize, FontWeight fontWeight,
      {Color color = Colors.black,double height = 1,double latterSpacing = 0.0 }) {
    return TextStyle(
        height: height,
        letterSpacing: latterSpacing,
        fontFamily: 'Titillium Web Black',
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize);
  }

  static Widget wrapButton({Function? onTap}) {
    return Expanded(
      child: Align(
        alignment: Alignment.bottomRight,
        child: InkWell(
          onTap: () {
            onTap!();
          },
          child: Container(
            height: 60.0,
            width: 160,
            padding: EdgeInsets.only(left: 48.0, right: 48.0),
            margin: EdgeInsets.only(top: 16, right: 16.0, bottom: 80.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              color: AppColor.colorTheme,
            ),
            alignment: Alignment.center,
            child: Text(
              AppStrings.next,
              style: Utilities.setTextStyle(
                  AppFontWeight.subTitleText, AppFontWeight.semiBold,
                  color: AppColor.colorPrimary),
            ),
          ),
        ),
      ),
    );
  }

  static Future<String> downloadFile(String url, String fileName, String dir) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    try {
      myUrl = url+'/'+fileName;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = '$dir/$fileName';
        file = File(filePath);
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }

    return filePath;
  }

  static Future<String?> getDownloadPath() async {
    Directory? directory;
    try {
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = Directory('/storage/emulated/0/Download');
        // Put file in global download folder, if for an unknown reason it didn't exist, we fallback
        // ignore: avoid_slow_async_io
        if (!await directory.exists()) directory = await getExternalStorageDirectory();
      }
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    return directory?.path;
  }
}
