
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tfour/TFour/Login.dart';
import 'package:tfour/utility/SessionManager.dart';
import 'App.dart';
import 'TFour/Dashboard.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  Widget defaultWidget;
  var accessToken = await SessionManager.getStringData(SessionManager.ACCESS_TOKEN);
  defaultWidget = accessToken.isNotEmpty ? Dashboard() : Login();
  runApp(MyApp(defaultWidget: defaultWidget));
}
