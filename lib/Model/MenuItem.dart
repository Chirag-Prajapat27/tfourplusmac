import 'package:flutter/material.dart';

class MenuItem{

  String name='';
  IconData iconData;

  MenuItem(this.name, this.iconData);
}