/// id : "1"
/// pro_cat_id : "4"
/// proj_name : "testing"
/// proj_name_ar : "اختبارات"
/// proj_img_url : "uploads/projects/1.jpg"
/// proj_desc : "Project Description"
/// proj_desc_ar : "وصف المشروع"
/// pro_bg_url : "uploads/projects/1_bg.jpg"
/// s1_img_url : null
/// s2_img_url : null
/// s3_img_url : null
/// s4_img_url : null
/// pdf_1_url : null
/// pdf_2_url : null
/// pdf_3_url : null
/// pdf_4_url : null
///
/// Project category item
/// pro_cat_id: "1",
/// pro_cat_name: "Residential ",
/// pro_cat_name_ar: "انتريور"

class ProjectProducts {
  ProjectProducts({
      String? id, 
      String? proCatId, 
      String? projName, 
      String? projNameAr, 
      String? projImgUrl,
      String? projDesc, 
      String? projDescAr, 
      String? proBgUrl, 
      dynamic s1ImgUrl, 
      dynamic s2ImgUrl, 
      dynamic s3ImgUrl, 
      dynamic s4ImgUrl, 
      dynamic pdf1Url, 
      dynamic pdf2Url, 
      dynamic pdf3Url, 
      dynamic pdf4Url,
     String? pro_cat_id,
     String? pro_cat_name,
     String? pro_cat_name_ar
  }){
    _id = id;
    _proCatId = proCatId;
    _projName = projName;
    _projNameAr = projNameAr;
    _projImgUrl = projImgUrl;
    _projDesc = projDesc;
    _projDescAr = projDescAr;
    _proBgUrl = proBgUrl;
    _s1ImgUrl = s1ImgUrl;
    _s2ImgUrl = s2ImgUrl;
    _s3ImgUrl = s3ImgUrl;
    _s4ImgUrl = s4ImgUrl;
    _pdf1Url = pdf1Url;
    _pdf2Url = pdf2Url;
    _pdf3Url = pdf3Url;
    _pdf4Url = pdf4Url;
    _pro_cat_id = pro_cat_id;
    _pro_cat_name = pro_cat_name;
    _pro_cat_name_ar = pro_cat_name_ar;
}

  ProjectProducts.fromJson(dynamic json) {
    _id = json['id'];
    _proCatId = json['pro_cat_id'];
    _projName = json['proj_name'];
    _projNameAr = json['proj_name_ar'];
    _projImgUrl = json['proj_img_url'];
    _projDesc = json['proj_desc'];
    _projDescAr = json['proj_desc_ar'];
    _proBgUrl = json['pro_bg_url'];
    _s1ImgUrl = json['s1_img_url'];
    _s2ImgUrl = json['s2_img_url'];
    _s3ImgUrl = json['s3_img_url'];
    _s4ImgUrl = json['s4_img_url'];
    _pdf1Url = json['pdf_1_url'];
    _pdf2Url = json['pdf_2_url'];
    _pdf3Url = json['pdf_3_url'];
    _pdf4Url = json['pdf_4_url'];
    _pro_cat_id = json['pro_cat_id'];
    _pro_cat_name = json['pro_cat_name'];
    _pro_cat_name_ar = json['pro_cat_name_ar'];
  }
  String? _id;
  String? _proCatId;
  String? _projName;
  String? _projNameAr;
  String? _projImgUrl;
  String? _projDesc;
  String? _projDescAr;
  String? _proBgUrl;
  String? _s1ImgUrl;
  String? _s2ImgUrl;
  String? _s3ImgUrl;
  String? _s4ImgUrl;
  String? _pdf1Url;
  String? _pdf2Url;
  String? _pdf3Url;
  String? _pdf4Url;
  String? _pro_cat_id;
  String? _pro_cat_name;
  String? _pro_cat_name_ar;

  String? get id => _id;
  String? get proCatId => _proCatId;
  String? get projName => _projName;
  String? get projNameAr => _projNameAr;
  String? get projImgUrl => _projImgUrl;
  String? get projDesc => _projDesc;
  String? get projDescAr => _projDescAr;
  String? get proBgUrl => _proBgUrl;
  String? get s1ImgUrl => _s1ImgUrl;
  String? get s2ImgUrl => _s2ImgUrl;
  String? get s3ImgUrl => _s3ImgUrl;
  String? get s4ImgUrl => _s4ImgUrl;
  String? get pdf1Url => _pdf1Url;
  String? get pdf2Url => _pdf2Url;
  String? get pdf3Url => _pdf3Url;
  String? get pdf4Url => _pdf4Url;
  String? get pro_cat_id => _pro_cat_id;
  String? get pro_cat_name => _pro_cat_name;
  String? get pro_cat_name_ar => _pro_cat_name_ar;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['pro_cat_id'] = _proCatId;
    map['proj_name'] = _projName;
    map['proj_name_ar'] = _projNameAr;
    map['proj_img_url'] = _projImgUrl;
    map['proj_desc'] = _projDesc;
    map['proj_desc_ar'] = _projDescAr;
    map['pro_bg_url'] = _proBgUrl;
    map['s1_img_url'] = _s1ImgUrl;
    map['s2_img_url'] = _s2ImgUrl;
    map['s3_img_url'] = _s3ImgUrl;
    map['s4_img_url'] = _s4ImgUrl;
    map['pdf_1_url'] = _pdf1Url;
    map['pdf_2_url'] = _pdf2Url;
    map['pdf_3_url'] = _pdf3Url;
    map['pdf_4_url'] = _pdf4Url;
    map['pro_cat_id'] = _pro_cat_id;
    map['pro_cat_name'] = _pro_cat_name;
    map['pro_cat_name_ar'] = _pro_cat_name_ar;
    return map;
  }

}