class   SubItemCategory {
  SubItemCategory({
      String? id, 
      String? proName, 
      String? proNameAr, 
      String? parCatId, 
      String? catId, 
      String? proDesc, 
      String? proDescAr, 
      String? proImgUrl, 
      String? proQuantity, 
      String? proDimension, 
      String? proBgUrl, 
      String? s1ImgUrl, 
      String? s2ImgUrl, 
      String? s3ImgUrl, 
      String? s4ImgUrl, 
      dynamic pdf1Url, 
      dynamic pdf2Url, 
      dynamic pdf3Url, 
      dynamic pdf4Url,}){
    _id = id;
    _proName = proName;
    _proNameAr = proNameAr;
    _parCatId = parCatId;
    _catId = catId;
    _proDesc = proDesc;
    _proDescAr = proDescAr;
    _proImgUrl = proImgUrl;
    _proQuantity = proQuantity;
    _proDimension = proDimension;
    _proBgUrl = proBgUrl;
    _s1ImgUrl = s1ImgUrl;
    _s2ImgUrl = s2ImgUrl;
    _s3ImgUrl = s3ImgUrl;
    _s4ImgUrl = s4ImgUrl;
    _pdf1Url = pdf1Url;
    _pdf2Url = pdf2Url;
    _pdf3Url = pdf3Url;
    _pdf4Url = pdf4Url;
}

  SubItemCategory.fromJson(dynamic json) {
    _id = json['id'];
    _proName = json['pro_name'];
    _proNameAr = json['pro_name_ar'];
    _parCatId = json['par_cat_id'];
    _catId = json['cat_id'];
    _proDesc = json['pro_desc'];
    _proDescAr = json['pro_desc_ar'];
    _proImgUrl = json['pro_img_url'];
    _proQuantity = json['pro_quantity'];
    _proDimension = json['pro_dimension'];
    _proBgUrl = json['pro_bg_url'];
    _s1ImgUrl = json['s1_img_url'];
    _s2ImgUrl = json['s2_img_url'];
    _s3ImgUrl = json['s3_img_url'];
    _s4ImgUrl = json['s4_img_url'];
    _pdf1Url = json['pdf_1_url'];
    _pdf2Url = json['pdf_2_url'];
    _pdf3Url = json['pdf_3_url'];
    _pdf4Url = json['pdf_4_url'];
  }
  String? _id;
  String? _proName;
  String? _proNameAr;
  String? _parCatId;
  String? _catId;
  String? _proDesc;
  String? _proDescAr;
  String? _proImgUrl;
  String? _proQuantity;
  String? _proDimension;
  String? _proBgUrl;
  String? _s1ImgUrl;
  String? _s2ImgUrl;
  String? _s3ImgUrl;
  String? _s4ImgUrl;
  dynamic _pdf1Url;
  dynamic _pdf2Url;
  dynamic _pdf3Url;
  dynamic _pdf4Url;

  String? get id => _id;
  String? get proName => _proName;
  String? get proNameAr => _proNameAr;
  String? get parCatId => _parCatId;
  String? get catId => _catId;
  String? get proDesc => _proDesc;
  String? get proDescAr => _proDescAr;
  String? get proImgUrl => _proImgUrl;
  String? get proQuantity => _proQuantity;
  String? get proDimension => _proDimension;
  String? get proBgUrl => _proBgUrl;
  String? get s1ImgUrl => _s1ImgUrl;
  String? get s2ImgUrl => _s2ImgUrl;
  String? get s3ImgUrl => _s3ImgUrl;
  String? get s4ImgUrl => _s4ImgUrl;
  dynamic get pdf1Url => _pdf1Url;
  dynamic get pdf2Url => _pdf2Url;
  dynamic get pdf3Url => _pdf3Url;
  dynamic get pdf4Url => _pdf4Url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['pro_name'] = _proName;
    map['pro_name_ar'] = _proNameAr;
    map['par_cat_id'] = _parCatId;
    map['cat_id'] = _catId;
    map['pro_desc'] = _proDesc;
    map['pro_desc_ar'] = _proDescAr;
    map['pro_img_url'] = _proImgUrl;
    map['pro_quantity'] = _proQuantity;
    map['pro_dimension'] = _proDimension;
    map['pro_bg_url'] = _proBgUrl;
    map['s1_img_url'] = _s1ImgUrl;
    map['s2_img_url'] = _s2ImgUrl;
    map['s3_img_url'] = _s3ImgUrl;
    map['s4_img_url'] = _s4ImgUrl;
    map['pdf_1_url'] = _pdf1Url;
    map['pdf_2_url'] = _pdf2Url;
    map['pdf_3_url'] = _pdf3Url;
    map['pdf_4_url'] = _pdf4Url;
    return map;
  }

}