import 'dart:convert';
/// id : "1"
/// about_us_content : "T Four Plus was established in the year 2013 and is a successful company in Kuwait, with varied business activities in the area of supplying material. T Four Plus is dedicated to achieving our corporate vision of commitment to providing innovative, practical and top-quality logistic services that improved business solution.\r\nOver the past years,innovation has been the cornerstone of T FourPlus.It has been the foundation of our success and it will continue to be the foundation for our future\r\n"
/// mission_text : "Our mission is to create reward experience and value that combine functionality with high quality and innovation for our customers. We want every partner and client experience to be the talk of a life time."
/// vision_text : "Our mission aim to shape spaces with “timeless designs” and want to be a globally recognized professional company, providing solutions in the field of high-end corporate, commercial & residential interiors"
/// about_img_url : "uploads/team_members/.jpg"
/// about_ar : "تهدف رؤيتنا إلى تشكيل المساحات \"بتصميمات خالدة\" ونريد أن نكون شركة محترفة معترف بها عالميًا ، تقدم حلولًا في مجال التصميمات الداخلية للشركات والتجارية والسكنية الراقية."
/// vision_text_ar : "مهمتنا هي خلق تجربة مكافأة وقيمة تجمع بين الوظائف والجودة العالية والابتكار لعملائنا. نريد أن تكون تجربة كل شريك وعميل حديث مدى الحياة."
/// mission_text_ar : "مهمتنا هي خلق تجربة مكافأة وقيمة تجمع بين الوظائف والجودة العالية والابتكار لعملائنا. نريد أن تكون تجربة كل شريك وعميل حديث مدى الحياة."


class AboutUsModel {
  AboutUsModel({
      String? id, 
      String? aboutUsContent, 
      String? missionText, 
      String? visionText, 
      String? aboutImgUrl, 
      String? aboutAr, 
      String? visionTextAr, 
      String? missionTextAr,}){
    _id = id;
    _aboutUsContent = aboutUsContent;
    _missionText = missionText;
    _visionText = visionText;
    _aboutImgUrl = aboutImgUrl;
    _aboutAr = aboutAr;
    _visionTextAr = visionTextAr;
    _missionTextAr = missionTextAr;
}

  AboutUsModel.fromJson(dynamic json) {
    _id = json['id'];
    _aboutUsContent = json['about_us_content'];
    _missionText = json['mission_text'];
    _visionText = json['vision_text'];
    _aboutImgUrl = json['about_img_url'];
    _aboutAr = json['about_ar'];
    _visionTextAr = json['vision_text_ar'];
    _missionTextAr = json['mission_text_ar'];
  }
  String? _id;
  String? _aboutUsContent;
  String? _missionText;
  String? _visionText;
  String? _aboutImgUrl;
  String? _aboutAr;
  String? _visionTextAr;
  String? _missionTextAr;
AboutUsModel copyWith({  String? id,
  String? aboutUsContent,
  String? missionText,
  String? visionText,
  String? aboutImgUrl,
  String? aboutAr,
  String? visionTextAr,
  String? missionTextAr,
}) => AboutUsModel(  id: id ?? _id,
  aboutUsContent: aboutUsContent ?? _aboutUsContent,
  missionText: missionText ?? _missionText,
  visionText: visionText ?? _visionText,
  aboutImgUrl: aboutImgUrl ?? _aboutImgUrl,
  aboutAr: aboutAr ?? _aboutAr,
  visionTextAr: visionTextAr ?? _visionTextAr,
  missionTextAr: missionTextAr ?? _missionTextAr,
);
  String? get id => _id;
  String? get aboutUsContent => _aboutUsContent;
  String? get missionText => _missionText;
  String? get visionText => _visionText;
  String? get aboutImgUrl => _aboutImgUrl;
  String? get aboutAr => _aboutAr;
  String? get visionTextAr => _visionTextAr;
  String? get missionTextAr => _missionTextAr;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['about_us_content'] = _aboutUsContent;
    map['mission_text'] = _missionText;
    map['vision_text'] = _visionText;
    map['about_img_url'] = _aboutImgUrl;
    map['about_ar'] = _aboutAr;
    map['vision_text_ar'] = _visionTextAr;
    map['mission_text_ar'] = _missionTextAr;
    return map;
  }

}