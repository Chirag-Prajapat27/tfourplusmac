/// id : "1"
/// parentCategory : "Doors"
/// categoryName : "Exterior Doors"
/// CategoryNameArabic : "أبواب خارجية"
/// imageUrl : "uploads/cat_img/1.jpg"
///
/////Slider data
/// id: "1",
/// slider_img_url: "uploads/slider/1.jpg",
/// slider_desc: "Made in Italy Entrances \r\nDesigned by You ",
///slider_desc_ar: "الاستمتاع بمساحة خارجية خلال فصل الشتاء ،"


class MainCategories {
  MainCategories({
      String? id, 
      String? parentCategory, 
      String? subCategoryName,
      String? categoryName,
      String? categoryNameArabic,
      String? imageUrl,
      String? slider_img_url,
      String? slider_desc,
      String? slider_desc_ar,
      }){


    _id = id;
    _parentCategory = parentCategory;
    _subCategoryName = subCategoryName;
    _categoryName = categoryName;
    _categoryNameArabic = categoryNameArabic;
    _imageUrl = imageUrl;
    _slider_img_url = slider_img_url;
    _slider_desc = slider_desc;
    _slider_desc_ar = slider_desc_ar;


}

  MainCategories.fromJson(dynamic json) {
    _id = json['id'];
    _parentCategory = json['parentCategory'];
    _subCategoryName = json['categoryName'];
    _categoryName = json['CategoryName'];
    _categoryNameArabic = json['CategoryNameArabic'];
    _imageUrl = json['imageUrl'];
    _slider_img_url = json['slider_img_url'];
    _slider_desc = json['slider_desc'];
    _slider_desc_ar = json['slider_desc_ar'];
  }
  String? _id;
  String? _parentCategory;
  String? _subCategoryName;
  String? _categoryName;
  String? _categoryNameArabic;
  String? _imageUrl;
  String? _slider_img_url;
  String? _slider_desc;
  String? _slider_desc_ar;


  String? get id => _id;
  String? get parentCategory => _parentCategory;
  String? get subCategoryName => _subCategoryName;
  String? get categoryName => _categoryName;
  String? get categoryNameArabic => _categoryNameArabic;
  String? get imageUrl => _imageUrl;
  String? get slider_img_url => _slider_img_url;
  String? get slider_desc => _slider_desc;
  String? get slider_desc_ar => _slider_desc_ar;


  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['parentCategory'] = _parentCategory;
    map['categoryName'] = _subCategoryName;
    map['CategoryName'] = _categoryName;
    map['CategoryNameArabic'] = _categoryNameArabic;
    map['imageUrl'] = _imageUrl;
    map['slider_img_url'] = _slider_img_url;
    map['slider_desc'] = _slider_desc;
    map['slider_desc_ar'] = _slider_desc_ar;

    return map;
  }

}