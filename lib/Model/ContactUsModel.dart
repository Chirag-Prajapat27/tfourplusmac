import 'dart:convert';
/// id : "1"
/// address : "Shuwaikh Industrial Area, Block 1, Street 12, \r\nBuilding 81, Kuwait"
/// tel_phone_1 : "55545805"
/// tel_phone_2 : ""
/// e_mail : "info@tfourplus.com"
/// fb_link : "https://www.facebook.com/tfourplus/"
/// twitter_link : "https://twitter.com/tfourplus?lang=en"
/// youtube_link : "https://www.youtube.com/"
/// whatsapp_number : "+96555545805"
/// address_ar : "منطقة الشويخ الصناعية ، قطعة 1 ، شارع 4 ، القطعة 21.؟ مدينة الكويت ، الكويت"

ContactUsModel contactUsModelFromJson(String str) => ContactUsModel.fromJson(json.decode(str));
String contactUsModelToJson(ContactUsModel data) => json.encode(data.toJson());
class ContactUsModel {
  ContactUsModel({
      String? id, 
      String? address, 
      String? telPhone1, 
      String? telPhone2, 
      String? eMail, 
      String? fbLink, 
      String? twitterLink, 
      String? youtubeLink, 
      String? whatsappNumber, 
      String? addressAr,}){
    _id = id;
    _address = address;
    _telPhone1 = telPhone1;
    _telPhone2 = telPhone2;
    _eMail = eMail;
    _fbLink = fbLink;
    _twitterLink = twitterLink;
    _youtubeLink = youtubeLink;
    _whatsappNumber = whatsappNumber;
    _addressAr = addressAr;
}

  ContactUsModel.fromJson(dynamic json) {
    _id = json['id'];
    _address = json['address'];
    _telPhone1 = json['tel_phone_1'];
    _telPhone2 = json['tel_phone_2'];
    _eMail = json['e_mail'];
    _fbLink = json['fb_link'];
    _twitterLink = json['twitter_link'];
    _youtubeLink = json['youtube_link'];
    _whatsappNumber = json['whatsapp_number'];
    _addressAr = json['address_ar'];
  }
  String? _id;
  String? _address;
  String? _telPhone1;
  String? _telPhone2;
  String? _eMail;
  String? _fbLink;
  String? _twitterLink;
  String? _youtubeLink;
  String? _whatsappNumber;
  String? _addressAr;
ContactUsModel copyWith({  String? id,
  String? address,
  String? telPhone1,
  String? telPhone2,
  String? eMail,
  String? fbLink,
  String? twitterLink,
  String? youtubeLink,
  String? whatsappNumber,
  String? addressAr,
}) => ContactUsModel(  id: id ?? _id,
  address: address ?? _address,
  telPhone1: telPhone1 ?? _telPhone1,
  telPhone2: telPhone2 ?? _telPhone2,
  eMail: eMail ?? _eMail,
  fbLink: fbLink ?? _fbLink,
  twitterLink: twitterLink ?? _twitterLink,
  youtubeLink: youtubeLink ?? _youtubeLink,
  whatsappNumber: whatsappNumber ?? _whatsappNumber,
  addressAr: addressAr ?? _addressAr,
);
  String? get id => _id;
  String? get address => _address;
  String? get telPhone1 => _telPhone1;
  String? get telPhone2 => _telPhone2;
  String? get eMail => _eMail;
  String? get fbLink => _fbLink;
  String? get twitterLink => _twitterLink;
  String? get youtubeLink => _youtubeLink;
  String? get whatsappNumber => _whatsappNumber;
  String? get addressAr => _addressAr;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['address'] = _address;
    map['tel_phone_1'] = _telPhone1;
    map['tel_phone_2'] = _telPhone2;
    map['e_mail'] = _eMail;
    map['fb_link'] = _fbLink;
    map['twitter_link'] = _twitterLink;
    map['youtube_link'] = _youtubeLink;
    map['whatsapp_number'] = _whatsappNumber;
    map['address_ar'] = _addressAr;
    return map;
  }

}